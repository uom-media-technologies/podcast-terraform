# Podcast Terraform

Defines and maintains the Podcast Service's cloud infrastructure. Using [Terraform](https://www.terraform.io) to define
[Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_Code). The infrastructure includes processing 
(Opencast), delivery (Videolounge), services (database, search) and monitoring (logging).


## Conventions

Structure should use generic terms for modules and filenames.

Keep Terraform object naming (variables, resources etc) concise.

Resource names are always used with the resource so don't over specify, e.g.

`resource "aws_ec2" "foo" {}` not `resource "aws_ec2" "ec2_foo" {}` because the resource is referenced as 
`${aws_ec2.foo}`.

Use underscores for Terraform objects (matches resource types etc) and hyphens for AWS names (S3 does not allow underscores for bucket names). e.g.

```
resource "aws_vcp" "foo_bar" {
  ...
  tags {
    Name = "foo-bar" # AWS name
  }
}
```

Module outputs should be named after the resource (less `aws_`) Terraform name and the key name e.g. 
`output "ebs_admin_id" {...`

If a variable or module output is a list the last parts should be pluralized e.g. `subnet_processing_ids`

## Development Notes

Recommend PyCharm (or any JetBrains based IDE) with the [HashiCorp/Terraform HCL](https://plugins.jetbrains.com/plugin/7808-hashicorp-terraform--hcl-language-support)
plugin. Does syntax checking and resource and variable auto-completion. 

Use [terraform-doc](https://github.com/segmentio/terraform-docs/releases) (>=13.0) to generate documentation. See also [.githooks/README.md](.githooks/README.md)

Consider [terragrunt](https://github.com/gruntwork-io/terragrunt) for environment configuration.

If you add a new module you need to rerun `terraform init`

## License

MIT, see LICENSE file.
