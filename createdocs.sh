#!/bin/sh

# requirements:
#   terraform-docs >= 0.13.0
# optional:
#   mkdocs >= 1.0.0

cat > mkdocs.yml <<EOF
---
site_name: "Podcast: Terraform"
repo_url: "https://bitbucket.org/uom-media-technologies/podcast-terraform/"
nav:
  - Home: README.md
  - Modules:
EOF

rm -rf docs site
mkdir docs
ln -s ../README.md docs/README.md

echo "Creating root module docs"
terraform-docs md --show header,providers,modules,inputs,outputs . > root.md
echo "    - root: 'root.md'" >> mkdocs.yml
ln -s ../root.md docs/root.md

for d in $(ls -1 modules)
do
  echo "Creating $d module docs"
  terraform-docs md modules/$d > modules/$d/README.md
  ln -s ../modules/$d/README.md docs/${d}.md
  echo "    - $d: '${d}.md'" >> mkdocs.yml
done

if command -v mkdocs > /dev/null
then
  mkdocs build
fi



