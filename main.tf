terraform {
  backend "remote" {
    organization = "MediaTechnologiesUoM"
    workspaces {
      prefix = "podcast-"
    }
  }
}

provider "aws" {
  region  = local.region
  profile = "${local.service}-${local.env}"
}

data "aws_caller_identity" "current" {
}

module "network" {
  source           = "./modules/network"
  prefix           = "${local.service}-${local.env}"
  env              = local.env
  region           = local.region
  zones            = local.zones
  uni_cidrs        = local.uni_cidrs
  resource_cidrs   = local.resource_cidrs
  monitoring_cidrs = local.monitoring_cidrs
}

module "security" {
  source  = "./modules/security"
  prefix  = "${local.service}-${local.env}"
  env     = local.env
  region  = local.region
  zones   = local.zones
  account = data.aws_caller_identity.current.account_id

  vpc_id         = module.network.vpc_id
  vpc_cidr       = module.network.vpc_cidr
  uni_cidrs      = local.uni_cidrs
  resource_cidrs = local.resource_cidrs

  hosted_zone_name = local.parent_domain

  bastion_public_key = file("files/keys/${local.service}-${local.env}-bastion.pub")
  access_public_key  = file("files/keys/${local.service}-${local.env}-access.pub")
}

module "storage" {
  source               = "./modules/storage"
  prefix               = "${local.service}-${local.env}"
  env                  = local.env
  region               = local.region
  zones                = local.zones
  account              = data.aws_caller_identity.current.account_id
  uni_security_account = data.aws_caller_identity.current.account_id # CHANGE ME
  subnet_ids           = module.network.subnet_ids

  video_index_snapshot_id = "snap-0c7324e920017ab2a"

  certificate_arns = module.security.certificate_arns

  distribution_cors_domains = formatlist("https://%s", concat(["video.${local.env_domain}"], local.uni_domains))

  cloudfront_public_keys = [ { date = 210428, public_key = file("files/keys/${local.service}-${local.env}-cloudfront-public-key-210428.pem")} ]
}

module "data" {
  source = "./modules/data"
  prefix = "${local.service}-${local.env}"
  env    = local.env
  region = local.region
  zones  = local.zones

  database_subnet_group_ids = concat(
    module.network.subnet_ids.processing,
    module.network.subnet_ids.delivery
  )
}

module "compute" {
  source = "./modules/compute"
  prefix = "${local.service}-${local.env}"
  env    = local.env
  region = local.region
  zones  = local.zones

  vpc_id                   = module.network.vpc_id
  vpc_cidr                 = module.network.vpc_cidr
  subnet_ids               = module.network.subnet_ids

  security_group_ids = module.security.security_group_ids
  profile_names      = module.security.profile_names
  certificate_arns   = module.security.certificate_arns

  efs_share_points              = module.storage.efs_share_points
  ebs_opencast_index_id         = module.storage.ebs_opencast_index_id
  ebs_video_index_ids           = module.storage.ebs_video_index_ids
  s3_archive_id                 = module.storage.s3_archive_id
  s3_distribution_id            = module.storage.s3_distribution_id

  // Changes to defaults
  video_count = length(module.storage.ebs_video_index_ids)

  tools_volume_size = 20

  loadbalance_external_timeout = 120

  processing_dependences = [module.storage.efs_dependence]

  waf_public_arn = module.security.waf_arn

  opencast_server = "http://admin.${local.env}.media.its.manchester.ac.uk"
}

module "domains" {
  source = "./modules/domains"
  prefix = "${local.service}-${local.env}"
  env    = local.env
  region = local.region

  vpc_id = module.network.vpc_id

  lb_public_https_arn     = module.compute.lb_public_https_arn
  lb_public_dns_name      = module.compute.lb_public_dns_name
  lb_public_zone_id       = module.compute.lb_public_zone_id
  lb_restricted_https_arn = module.compute.lb_restricted_https_arn
  lb_restricted_dns_name  = module.compute.lb_restricted_dns_name
  lb_restricted_zone_id   = module.compute.lb_restricted_zone_id
  lb_private_dns_name     = module.compute.lb_private_dns_name
  lb_private_zone_id      = module.compute.lb_private_zone_id

  cloudfront_distribution_domain = module.storage.cloudfront_distribution_domain

  database_dns_name = split(":", module.data.database_endpoint)[0]

  ops_ip          = module.compute.ops_ip
  ops_ip_internal = module.compute.ops_ip_internal
  log_ip          = module.compute.log_ip
  log_ip_internal = module.compute.log_ip_internal

  admin_standby  = false # required as length of admin_ips can't be computed
  admin_ips      = module.compute.admin_ips
  admin_ids      = module.compute.admin_ids
  index_ips      = module.compute.index_ips

  hosted_zone_name = local.parent_domain
}

