#Useful Git Hooks

##Install
`ln -sf ../../.githooks/<hook-name> .git/hooks`

##Hooks
* post-checkout - this will call `select_env.sh` with the appropriate environment value based on the branch name (develop, staging and production only)
* pre-commit - attempts to update the module README.md files by calling `createdocs.sh`, requires terraform-docs >= 0.13

