## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_compute"></a> [compute](#module\_compute) | ./modules/compute |  |
| <a name="module_data"></a> [data](#module\_data) | ./modules/data |  |
| <a name="module_domains"></a> [domains](#module\_domains) | ./modules/domains |  |
| <a name="module_network"></a> [network](#module\_network) | ./modules/network |  |
| <a name="module_security"></a> [security](#module\_security) | ./modules/security |  |
| <a name="module_storage"></a> [storage](#module\_storage) | ./modules/storage |  |

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ops_ip"></a> [ops\_ip](#output\_ops\_ip) | n/a |
