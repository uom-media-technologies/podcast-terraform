# storage/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

variable "primary_zone_index" {
  default     = 0
  description = "Index of the primary availability zone to use"
}

variable "account" {
  description = "Account number, for ARN calculation"
}

variable "uni_security_account" {
  description = "University security account number, for ARN calculation"
}

# network inputs
variable "subnet_ids" {
  type = object({
    public     = list(string)
    restricted = list(string)
    processing = list(string)
    delivery   = list(string)
  })
}

# SSL Certificates
variable "certificate_arns" {
  type = object({
    distribution = string
  })
  description = "ARNs of SSL certificates"
}

# domains for CORS
variable "distribution_cors_domains" {
  type = list(string)
  description ="Domain pattern for CORS to S3 distribution bucket"
}

# storage configuration

# block storage
# ebs sizes in GB
variable "opencast_index_size" {
  default     = 5
  description = "Size of Opencast Index storage in GB"
}

variable "video_count" {
  default     = 1
  description = "Number of Video Portal instances (static)"
}

variable "video_index_size" {
  default     = 8
  description = "Size of Video Index storage in GB"
}

variable "video_index_snapshot_id" {
  description = "Id of the EBS snapshot to use for the Video Index"
}

# object storage
variable "s3_delivery_transition" {
  default     = 60 # days
  description = "Days before moving published objects to Intelligent Tiering"
}

variable "s3_archive_transition" {
  default     = 7 # days
  description = "Days before moving archived objects to Intelligent Tiering"
}

variable "s3_archive_backup_transition" {
  default     = 30 # days
  description = "Days before moving backup archived objects to Glacier"
}

variable "archive_live_colour" {
  default = "blue"
  description = "The colour of archive live resources, either blue or green"
}

variable "distribution_live_colour" {
  default = "blue"
  description = "The colour of live distribution resources, either blue or green"
}

# cloudfront
variable "cloudfront_public_keys" {
  type = list(object({
    date = number
    public_key = string
  }))
  default = []
  description = "The current and the deprecated public keys"
}