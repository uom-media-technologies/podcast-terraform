/**
 * # Storage
 *
 * Define block, file and object storage as well as their access policies. EBS type and size define as locals,
 * see storage/main.tf
 *
 * ## Usage:
 * ```
 * module "storage" {
 *   source = "./storage"
 *   prefix = "podcast-prod"
 *   env = "prod"
 *   region = "us-east"
 *   zones = ["us-east-1", "us-west-1"]
 *   account = "12345678901"
 *
 *   subnet_public_ids = "${module.network.subnet_public_ids}"
 *   subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
 *   subnet_processing_ids = "${module.network.subnet_processing_ids}"
 *   subnet_delivery_ids = "${module.network.subnet_delivery_ids}"
 *
 *   opencast_index_size = 5
 *   s3_delivery_transition = 60 # days
 *   s3_archive_transition = 7 # days
 * }
 * ```
 */


# indices ebs type
locals {
  index_type       = "gp2"
  video_index_type = "gp3"
}

locals {
  s3_delete_contents = var.env == "prod" ? false : true
}

