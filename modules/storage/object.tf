# storage/block.tf

# live and backup resources are treated as interchangeable blue-green entities
#
# Distribution
#

# 'blue' instance, implicit, initially live
resource "aws_s3_bucket" "distribution" {
  bucket        = "${var.prefix}-distribution"
  acl           = "private" # This is not the default ACL for objects
  force_destroy = local.s3_delete_contents
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = var.distribution_cors_domains
    max_age_seconds = 3600
  }
  lifecycle_rule {
    id      = "live"
    enabled = var.distribution_live_colour == "blue"
    transition {
      days          = var.s3_delivery_transition
      storage_class = "INTELLIGENT_TIERING"
    }
  }
  # Note: no lifecycle rule for id = "backup"
  # Objects are expected to be stored as Infrequent Access
  tags = {
    Name      = "${var.prefix}-distribution"
    App       = "opencast"
    Component = "distribution"
    Colour    = "blue"
    Status    = var.distribution_live_colour == "blue" ? "live" : "backup"
    Env       = var.env
  }
}

resource "aws_s3_bucket_policy" "distribution" {
  bucket = var.distribution_live_colour == "blue" ? aws_s3_bucket.distribution.id : aws_s3_bucket.distribution_green.id
  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      %{ if var.env != "dev" ~}
      {
        "Sid": "BucketDeletionPrevention",
        "Action": "s3:DeleteBucket",
        "Effect": "Deny",
        "Principal": {
          "AWS": "*"
        },
        "Resource": "arn:aws:s3:::${var.prefix}-distribution%{if var.distribution_live_colour == "green"}-green%{endif}"
      },
      %{ endif ~}
      {
        "Sid": "CloudfrontReadOnly",
        "Action": "s3:GetObject",
        "Effect": "Allow",
        "Principal": {
          "AWS": "${aws_cloudfront_origin_access_identity.distribution.iam_arn}"
        },
        "Resource": "arn:aws:s3:::${var.prefix}-distribution%{if var.distribution_live_colour == "green"}-green%{endif}/*"
      },
      {
        "Sid": "PublicReadOnly",
        "Action": "s3:GetObject",
        "Effect": "Allow",
        "Principal": "*",
        "Resource": "arn:aws:s3:::${var.prefix}-distribution%{if var.distribution_live_colour == "green"}-green%{endif}/*",
        "Condition": {
          "StringEquals": {
             "s3:ExistingObjectTag/Public": "true"
          }
        }
      }
    ]
  }
  EOF

}

# 'green' instance, initially backup
resource "aws_s3_bucket" "distribution_green" {
  bucket        = "${var.prefix}-distribution-green"
  acl           = "private"
  force_destroy = local.s3_delete_contents
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = var.distribution_cors_domains
    max_age_seconds = 3600
  }
  lifecycle_rule {
    id      = "live"
    enabled = var.distribution_live_colour == "green"
    transition {
      days          = var.s3_delivery_transition
      storage_class = "INTELLIGENT_TIERING"
    }
  }
  # Note: no lifecycle rule for id = "backup"
  # Objects are expected to be stored as Infrequent Access
  tags = {
    Name      = "${var.prefix}-distribution-green"
    App       = "opencast"
    Component = "distribution"
    Colour    = "green"
    Status    = var.distribution_live_colour == "blue" ? "backup" : "live"
    Env       = var.env
  }
}

resource "aws_s3_bucket_policy" "distribution_backup" {
  bucket = var.distribution_live_colour == "blue" ? aws_s3_bucket.distribution_green.id : aws_s3_bucket.distribution.id
  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
       {
        "Sid": "DeletionProtection",
        "Action": [
          %{if var.env != "dev"}"s3:DeleteBucket",%{endif}
          "s3:DeleteObject"
        ],
        "Effect": "Deny",
        "Principal": "*",
        "Resource": [
          %{if var.env != "dev"}"arn:aws:s3:::${var.prefix}-distribution%{if var.distribution_live_colour == "blue"}-green%{endif}",%{endif}
          "arn:aws:s3:::${var.prefix}-distribution%{if var.distribution_live_colour == "blue"}-green%{endif}/*"
        ]
      }
    ]
  }
  EOF

}

#
# Archive
#

# 'blue' instance, implicit
resource "aws_s3_bucket" "archive" {
  bucket        = "${var.prefix}-archive"
  acl           = "private"
  force_destroy = local.s3_delete_contents
  lifecycle_rule {
    id      = "live"
    enabled = var.archive_live_colour == "blue"
    transition {
      days          = var.s3_archive_transition
      storage_class = "INTELLIGENT_TIERING"
    }
  }
  lifecycle_rule {
    id      = "backup"
    enabled = var.archive_live_colour == "green"
    transition {
      days          = var.s3_archive_transition
      storage_class = "GLACIER"
    }
  }
  tags = {
    Name      = "${var.prefix}-archive"
    App       = "opencast"
    Component = "archive"
    Colour    = "blue"
    Status    = var.archive_live_colour == "blue" ? "live" : "backup"
    Env       = var.env
  }
}

resource "aws_s3_bucket_policy" "archive" {
  count = var.env != "dev" ? 1 :0
  bucket = var.archive_live_colour == "blue" ? aws_s3_bucket.archive.id : aws_s3_bucket.archive_green.id
  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "BucketDeletionPrevention",
        "Action": "s3:DeleteBucket",
        "Effect": "Deny",
        "Principal": {
          "AWS": "*"
        },
        "Resource": "arn:aws:s3:::${var.prefix}-archive%{if var.archive_live_colour == "green"}-green%{endif}"
      }
    ]
  }
  EOF

}

# 'green' instance, initially backup
resource "aws_s3_bucket" "archive_green" {
  bucket        = "${var.prefix}-archive-green"
  acl           = "private"
  force_destroy = local.s3_delete_contents
  lifecycle_rule {
    id = "live"
    enabled = var.archive_live_colour == "green"
    transition {
      days          = var.s3_archive_transition
      storage_class = "INTELLIGENT_TIERING"
    }
  }
  lifecycle_rule {
    id = "backup"
    enabled = var.archive_live_colour == "blue"
    transition {
      days          = var.s3_archive_transition
      storage_class = "GLACIER"
    }
  }
  tags = {
    Name      = "${var.prefix}-archive-green"
    App       = "opencast"
    Component = "archive"
    Colour    = "green"
    Status    = var.archive_live_colour == "blue" ? "backup" : "live"
    Env       = var.env
  }
}

resource "aws_s3_bucket_policy" "archive_backup" {
  bucket = var.archive_live_colour == "blue" ? aws_s3_bucket.archive_green.id : aws_s3_bucket.archive.id
  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "DeletionProtection",
        "Action": [
          %{if var.env != "dev"}"s3:DeleteBucket",%{endif}
          "s3:DeleteObject"
        ],
        "Effect": "Deny",
        "Principal": "*",
        "Resource": [
          %{if var.env != "dev"}"arn:aws:s3:::${var.prefix}-archive%{if var.archive_live_colour == "blue"}-green%{endif}",%{endif}
          "arn:aws:s3:::${var.prefix}-archive%{if var.archive_live_colour == "blue"}-green%{endif}/*"
        ]
      }
    ]
  }
  EOF

}

#
# EBS Snapshots
#
resource "aws_s3_bucket" "snapshots" {
  bucket        = "${var.prefix}-snapshots"
  acl           = "private"
  force_destroy = local.s3_delete_contents
  tags = {
    Name      = "${var.prefix}-snapshots"
    Env       = var.env
    Component = "backup"
  }
}

#
# Logs
#
data "aws_s3_bucket" "logs" {
  bucket = "media-technologies-logs"
}

