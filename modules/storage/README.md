# Storage

Define block, file and object storage as well as their access policies. EBS type and size define as locals,
see storage/main.tf

## Usage:
```
module "storage" {
  source = "./storage"
  prefix = "podcast-prod"
  env = "prod"
  region = "us-east"
  zones = ["us-east-1", "us-west-1"]
  account = "12345678901"

  subnet_public_ids = "${module.network.subnet_public_ids}"
  subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
  subnet_processing_ids = "${module.network.subnet_processing_ids}"
  subnet_delivery_ids = "${module.network.subnet_delivery_ids}"

  opencast_index_size = 5
  s3_delivery_transition = 60 # days
  s3_archive_transition = 7 # days
}
```

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_origin_request_policy.origin_request](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/cloudfront_origin_request_policy) | data source |
| [aws_s3_bucket.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/s3_bucket) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account"></a> [account](#input\_account) | Account number, for ARN calculation | `any` | n/a | yes |
| <a name="input_archive_live_colour"></a> [archive\_live\_colour](#input\_archive\_live\_colour) | The colour of archive live resources, either blue or green | `string` | `"blue"` | no |
| <a name="input_certificate_arns"></a> [certificate\_arns](#input\_certificate\_arns) | ARNs of SSL certificates | <pre>object({<br>    distribution = string<br>  })</pre> | n/a | yes |
| <a name="input_cloudfront_public_keys"></a> [cloudfront\_public\_keys](#input\_cloudfront\_public\_keys) | The current and the deprecated public keys | <pre>list(object({<br>    date = number<br>    public_key = string<br>  }))</pre> | `[]` | no |
| <a name="input_distribution_cors_domains"></a> [distribution\_cors\_domains](#input\_distribution\_cors\_domains) | Domain pattern for CORS to S3 distribution bucket | `list(string)` | n/a | yes |
| <a name="input_distribution_live_colour"></a> [distribution\_live\_colour](#input\_distribution\_live\_colour) | The colour of live distribution resources, either blue or green | `string` | `"blue"` | no |
| <a name="input_env"></a> [env](#input\_env) | Deployed environment | `any` | n/a | yes |
| <a name="input_opencast_index_size"></a> [opencast\_index\_size](#input\_opencast\_index\_size) | Size of Opencast Index storage in GB | `number` | `5` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resource naming prefix | `any` | n/a | yes |
| <a name="input_primary_zone_index"></a> [primary\_zone\_index](#input\_primary\_zone\_index) | Index of the primary availability zone to use | `number` | `0` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `any` | n/a | yes |
| <a name="input_s3_archive_backup_transition"></a> [s3\_archive\_backup\_transition](#input\_s3\_archive\_backup\_transition) | Days before moving backup archived objects to Glacier | `number` | `30` | no |
| <a name="input_s3_archive_transition"></a> [s3\_archive\_transition](#input\_s3\_archive\_transition) | Days before moving archived objects to Intelligent Tiering | `number` | `7` | no |
| <a name="input_s3_delivery_transition"></a> [s3\_delivery\_transition](#input\_s3\_delivery\_transition) | Days before moving published objects to Intelligent Tiering | `number` | `60` | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | network inputs | <pre>object({<br>    public     = list(string)<br>    restricted = list(string)<br>    processing = list(string)<br>    delivery   = list(string)<br>  })</pre> | n/a | yes |
| <a name="input_uni_security_account"></a> [uni\_security\_account](#input\_uni\_security\_account) | University security account number, for ARN calculation | `any` | n/a | yes |
| <a name="input_video_count"></a> [video\_count](#input\_video\_count) | Number of Video Portal instances (static) | `number` | `1` | no |
| <a name="input_video_index_size"></a> [video\_index\_size](#input\_video\_index\_size) | Size of Video Index storage in GB | `number` | `8` | no |
| <a name="input_video_index_snapshot_id"></a> [video\_index\_snapshot\_id](#input\_video\_index\_snapshot\_id) | Id of the EBS snapshot to use for the Video Index | `any` | n/a | yes |
| <a name="input_zones"></a> [zones](#input\_zones) | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudfront_distribution_domain"></a> [cloudfront\_distribution\_domain](#output\_cloudfront\_distribution\_domain) | Domain name of the cloudfront for distribution (not alias) |
| <a name="output_ebs_opencast_index_id"></a> [ebs\_opencast\_index\_id](#output\_ebs\_opencast\_index\_id) | ebs ids |
| <a name="output_ebs_video_index_ids"></a> [ebs\_video\_index\_ids](#output\_ebs\_video\_index\_ids) | n/a |
| <a name="output_efs_dependence"></a> [efs\_dependence](#output\_efs\_dependence) | Dummy varaible with dependence on efs filesystem |
| <a name="output_efs_share_points"></a> [efs\_share\_points](#output\_efs\_share\_points) | EFS DNS name that will resolve to mount target IP address in the AZ |
| <a name="output_s3_archive_id"></a> [s3\_archive\_id](#output\_s3\_archive\_id) | S3 Opencast Archive bucket id |
| <a name="output_s3_distribution_id"></a> [s3\_distribution\_id](#output\_s3\_distribution\_id) | S3 Opencast Distribution bucket id |
| <a name="output_s3_logs_id"></a> [s3\_logs\_id](#output\_s3\_logs\_id) | S3 Logs bucket id |
| <a name="output_s3_snapshot_id"></a> [s3\_snapshot\_id](#output\_s3\_snapshot\_id) | S3 EBS snapshot bucket id |
