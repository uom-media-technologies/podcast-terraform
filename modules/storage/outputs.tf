# storage/outputs.tf

# ebs ids
output "ebs_opencast_index_id" {
  value = aws_ebs_volume.opencast_index.id
}

output "ebs_video_index_ids" {
  value = aws_ebs_volume.video_index.*.id
}

# efs mount targets
output "efs_share_points" {
  value = ["${aws_efs_mount_target.private[0].dns_name}:/", "${aws_efs_mount_target.private[1].dns_name}:/"]
  description = "EFS DNS name that will resolve to mount target IP address in the AZ"
}

output "efs_dependence" {
  value = {}
  depends_on = [aws_efs_file_system.work]
  description = "Dummy varaible with dependence on efs filesystem"
}

# s3 ids
output "s3_snapshot_id" {
  value       = aws_s3_bucket.snapshots.id
  description = "S3 EBS snapshot bucket id"
}

output "s3_logs_id" {
  value       = data.aws_s3_bucket.logs.id
  description = "S3 Logs bucket id"
}

output "s3_distribution_id" {
  value       = var.distribution_live_colour == "blue" ? aws_s3_bucket.distribution.id : aws_s3_bucket.distribution_green.id
  description = "S3 Opencast Distribution bucket id"
}

output "s3_archive_id" {
  value       =  var.archive_live_colour == "blue" ? aws_s3_bucket.archive.id : aws_s3_bucket.archive_green.id
  description = "S3 Opencast Archive bucket id"
}

output "cloudfront_distribution_domain" {
  value       = aws_cloudfront_distribution.distribution.domain_name
  description = "Domain name of the cloudfront for distribution (not alias)"
}
