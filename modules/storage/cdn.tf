# storage/cdn.tf

resource aws_cloudfront_public_key "distribution"  {
  for_each    = { for key in var.cloudfront_public_keys : key.date => key }
  name        = "${var.prefix}-${each.value.date}"
  encoded_key = each.value.public_key
  comment     = "${var.prefix} starting from ${each.value.date}"
  lifecycle {
    create_before_destroy = true
  }
}

resource aws_cloudfront_key_group "distribution" {
  name    = "${var.prefix}-distribution"
  items   = [for key in aws_cloudfront_public_key.distribution : key.id]
  comment = "Current and previous keys"
}

resource "aws_cloudfront_cache_policy" "cache" {
  name        = "${var.prefix}-distribution"
  comment     = "Custom policy to cache key on origin header"
  default_ttl = 86400
  max_ttl     = 31536000
  min_ttl     = 1
  parameters_in_cache_key_and_forwarded_to_origin {
    headers_config {
      header_behavior = "whitelist"
      headers {
        items = ["origin"]
      }
    }
    cookies_config {
      cookie_behavior = "none"
    }
    query_strings_config {
      query_string_behavior = "whitelist"
      query_strings {
        items = ["response-content-disposition"]
      }
    }
    enable_accept_encoding_brotli = false
    enable_accept_encoding_gzip   = false
  }
}

data aws_cloudfront_origin_request_policy "origin_request" {
  name = "Managed-CORS-S3Origin"
}

resource aws_cloudfront_origin_access_identity "distribution" {
  comment = "Access S3 ${var.prefix}-distribution bucket"
}

resource aws_cloudfront_response_headers_policy "distribution" {
  name = "${var.prefix}-distribution"

  custom_headers_config {
    items {
      header   = "Cache-Control"
      override = false
      value    = "max-age=31536000"
    }
  }
}

resource aws_cloudfront_distribution "distribution" {
  comment     = "CDN for podcast ${var.env} published videos"
  enabled     = true
  # 200, US, EU, Asia, Far East
  price_class = "PriceClass_200"
  origin {
    domain_name = var.distribution_live_colour == "blue" ? aws_s3_bucket.distribution.bucket_regional_domain_name : aws_s3_bucket.distribution_green.bucket_regional_domain_name
    origin_id   = "${var.prefix}-distribution"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.distribution.cloudfront_access_identity_path
    }
  }

  # OPTION: Origin groups to allow failover to another bucket

  default_cache_behavior {
    allowed_methods            = ["GET", "HEAD", "OPTIONS"]
    cached_methods             = ["GET", "HEAD"]
    cache_policy_id            = aws_cloudfront_cache_policy.cache.id
    target_origin_id           = "${var.prefix}-distribution"
    viewer_protocol_policy     = "https-only"
    trusted_key_groups         = [aws_cloudfront_key_group.distribution.id]
    origin_request_policy_id   = data.aws_cloudfront_origin_request_policy.origin_request.id
    response_headers_policy_id = aws_cloudfront_response_headers_policy.distribution.id
    # NOTE: IDE may complain about lack of forwarding_values block but the origin_request policy
    # provides those details and precludes its use
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  aliases     = ["distribution.${var.env}.media.its.manchester.ac.uk"]
  viewer_certificate {
    acm_certificate_arn = var.certificate_arns.distribution
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }
  tags = {
    Name      = "${var.prefix}-distribution"
    App       = "videolounge"
    Component = "distribution"
    Env       = var.env
  }
}



