# storage/block.tf

# Opencast index
resource "aws_ebs_volume" "opencast_index" {
  availability_zone = element(var.zones, var.primary_zone_index)
  type              = local.index_type
  size              = var.opencast_index_size
  tags = {
    Name      = "${var.prefix}-opencast-index"
    Env       = var.env
    Component = "index"
  }
}

# Video portal index
resource "aws_ebs_volume" "video_index" {
  count         = var.video_count
  availability_zone = element(var.zones, count.index % 2)
  type              = local.video_index_type
  snapshot_id       = var.video_index_snapshot_id
  tags = {
    Name      = "${var.prefix}-video-index-${count.index}"
    Env       = var.env
    Component = "index"
  }
}