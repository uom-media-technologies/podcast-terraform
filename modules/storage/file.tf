# storage/file.tf

#
# EFS
#
resource "aws_efs_file_system" "work" {
  creation_token = "${var.prefix}-work"
  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }
  tags = {
    Name      = "${var.prefix}-work"
    Env       = var.env
    Component = "workspace"
  }
}

# Create a mount target IP address in each AZ (not subnet)
# Actual mounting must be provisioned
resource "aws_efs_mount_target" "private" {
  # count = "${length(var.subnet_processing_ids)}" this type of express dependent on other resources is not yet supported
  # https://github.com/hashicorp/terraform/issues/12570
  count          = 2
  file_system_id = aws_efs_file_system.work.id
  subnet_id      = var.subnet_ids.processing[count.index]
}

