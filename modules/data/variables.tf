# data.variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

variable "primary_zone_index" {
  default     = 0
  description = "Primary availability zone"
}

# database
variable "database_type" {
  default     = "db.t3.small" # micro does not support encryption
  description = "Database instance type"
}

variable "database_storage_size" {
  default     = 10 # GB
  description = "Database storage size in GB"
}

variable "database_subnet_group_ids" {
  type        = list(string)
  description = "List of subnet ids to form a group that can access the database"
}

variable "database_backup_window" {
  default     = "03:10-03:40"
  description = "(Prod only) Backup time window in , format HH:MM-HH:MM in UTC. Don't overlap maintenance window"
}

variable "database_backup_retention" {
  default     = 7
  description = "(Prod only) Number of days to keep backups, 0 to disable"
}

variable "database_maintenance_window" {
  default     = "Sun:02:04-Sun:02:34"
  description = "Maintenance datetime window, format ddd:HH:MM-ddd:HH:MM in UTC. Don't overlap backup window"
}

variable "db_max_connections" {
  default     = "0"
  description = "Maximum number of database connections. Value of 0 means use system default"
}

