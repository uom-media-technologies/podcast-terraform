# compute/database.tf

data "aws_secretsmanager_secret_version" "database" {
  secret_id = "media/podcasting/${var.env}/database/rds"
}

resource "aws_db_subnet_group" "private" {
  name       = "${var.prefix}-private"
  subnet_ids = var.database_subnet_group_ids
  tags = {
    Env = var.env
  }
}

resource "aws_db_parameter_group" "podcast" {
  name   = "${var.prefix}-${local.database_engine}${replace(local.database_engine_major_version, ".", "-")}"
  family = local.database_family
  parameter {
    name  = "time_zone"
    value = local.database_timezone
  }
  parameter {
    name  = "log_bin_trust_function_creators"
    value = 1
  }
  dynamic "parameter" {
    for_each = var.db_max_connections > 0 ? [{ name = "max_connections", value = var.db_max_connections }] : []
    content {
      name  = parameter.value["name"]
      value = parameter.value["value"]
    }
  }
  tags = {
    Env       = var.env
    Component = "database"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_instance" "podcast" {
  identifier                = var.prefix
  engine                    = local.database_engine
  engine_version            = local.database_engine_version
  storage_type              = local.database_storage
  allocated_storage         = var.database_storage_size
  storage_encrypted         = true
  instance_class            = var.database_type
  db_subnet_group_name      = aws_db_subnet_group.private.name
  name                      = "podcast_${var.env}"
  username                  = jsondecode(data.aws_secretsmanager_secret_version.database.secret_string)["login"]
  password                  = jsondecode(data.aws_secretsmanager_secret_version.database.secret_string)["password"]
  parameter_group_name      = aws_db_parameter_group.podcast.name
  skip_final_snapshot       = var.env == "prod" ? false : true
  final_snapshot_identifier = "${var.prefix}-database-final"
  deletion_protection       = var.env == "prod" ? true : false
  maintenance_window        = var.database_maintenance_window
  backup_retention_period   = var.env == "prod" ? var.database_backup_retention : 0
  backup_window             = var.database_backup_window
  # uncomment to allow major version upgrades
  allow_major_version_upgrade = true
  apply_immediately         = true

  tags = {
    Env       = var.env
    App       = "mysql"
    Component = "database"
    host      = "database"
  }

  depends_on = [aws_db_parameter_group.podcast]
}

