# Data

Define all database and index services.

## Usage:
```
module "data" {
  source = "./data"
  prefix = "podcast-stage"
  env = "stage"
  region = "eu-west-1"
  zones = "eu-west-1a, eu-west-1b"

  database_type = "db.t2.micro"
  database_storage_size = 10 # GB
  database_subnet_group = [ "subnet_private_1_id", "subnet_private_2_id"]
}
```

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |

## Resources

| Name | Type |
|------|------|
| [aws_secretsmanager_secret_version.database](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_database_backup_retention"></a> [database\_backup\_retention](#input\_database\_backup\_retention) | (Prod only) Number of days to keep backups, 0 to disable | `number` | `7` | no |
| <a name="input_database_backup_window"></a> [database\_backup\_window](#input\_database\_backup\_window) | (Prod only) Backup time window in , format HH:MM-HH:MM in UTC. Don't overlap maintenance window | `string` | `"03:10-03:40"` | no |
| <a name="input_database_maintenance_window"></a> [database\_maintenance\_window](#input\_database\_maintenance\_window) | Maintenance datetime window, format ddd:HH:MM-ddd:HH:MM in UTC. Don't overlap backup window | `string` | `"Sun:02:04-Sun:02:34"` | no |
| <a name="input_database_storage_size"></a> [database\_storage\_size](#input\_database\_storage\_size) | Database storage size in GB | `number` | `10` | no |
| <a name="input_database_subnet_group_ids"></a> [database\_subnet\_group\_ids](#input\_database\_subnet\_group\_ids) | List of subnet ids to form a group that can access the database | `list(string)` | n/a | yes |
| <a name="input_database_type"></a> [database\_type](#input\_database\_type) | Database instance type | `string` | `"db.t3.small"` | no |
| <a name="input_db_max_connections"></a> [db\_max\_connections](#input\_db\_max\_connections) | Maximum number of database connections. Value of 0 means use system default | `string` | `"0"` | no |
| <a name="input_env"></a> [env](#input\_env) | Deployed environment | `any` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resource naming prefix | `any` | n/a | yes |
| <a name="input_primary_zone_index"></a> [primary\_zone\_index](#input\_primary\_zone\_index) | Primary availability zone | `number` | `0` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `any` | n/a | yes |
| <a name="input_zones"></a> [zones](#input\_zones) | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_database_endpoint"></a> [database\_endpoint](#output\_database\_endpoint) | Database endpoint |
