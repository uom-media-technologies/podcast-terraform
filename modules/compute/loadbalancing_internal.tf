# compute/loadbalancing_internal.tf

#
# Load Balancers
#

# Private
resource "aws_alb" "private" {
  name            = "${var.prefix}-private"
  internal        = true
  subnets         = var.subnet_ids.delivery
  security_groups = [var.security_group_ids.default]
  tags = {
    Env = var.env
  }
}

resource "aws_alb_listener" "private_http" {
  load_balancer_arn = aws_alb.private.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_alb_target_group.edit_internal.arn
    type             = "forward"
  }
}

#
# Internal Target Groups
#

# Edit
resource "aws_alb_target_group" "edit_internal" {
  name     = "${var.prefix}-edit-internal"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.opencast_health_check_path

    healthy_threshold   = var.loadbalance_healthy_threshold_slow
    unhealthy_threshold = var.loadbalance_unhealthy_threshold
    interval            = var.loadbalance_health_interval
    timeout             = var.loadbalance_health_timeout
  }
  stickiness {
    type    = "lb_cookie"
    cookie_duration = var.loadbalance_edit_internal_stickiness
    enabled = true
  }
  tags = {
    Env       = var.env
    App       = "opencast"
    Component = "oaipmh"
  }
}

resource "aws_alb_target_group_attachment" "edit_internal" {
  count            = var.edit_count
  target_group_arn = aws_alb_target_group.edit_internal.arn
  target_id        = aws_instance.edit[count.index].id
}


# Upload
resource "aws_alb_target_group" "upload_internal" {
  name     = "${var.prefix}-upload-internal"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.opencast_health_check_path

    healthy_threshold   = var.loadbalance_healthy_threshold_slow
    unhealthy_threshold = var.loadbalance_unhealthy_threshold
    interval            = var.loadbalance_health_interval
    timeout             = var.loadbalance_health_timeout
  }
  stickiness {
    type    = "lb_cookie"
    cookie_duration = var.loadbalance_upload_internal_stickiness
    enabled = true
  }
  tags = {
    Env       = var.env
    App       = "opencast"
    Component = "ingest"
  }
}

resource "aws_alb_listener_rule" "upload_internal" {
  depends_on   = [aws_alb_target_group.upload_internal]
  listener_arn = aws_alb_listener.private_http.arn
  priority     = 20
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.upload_internal.arn
  }
  condition {
    host_header {
      values = ["upload.*"]
    }
  }
}