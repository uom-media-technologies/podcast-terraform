# compute/loadbalancing_external.tf

#
# Load Balancers
#

# Public
resource "aws_alb" "public" {
  name     = "${var.prefix}-public"
  internal = false
  subnets         = var.subnet_ids.public
  security_groups = [var.security_group_ids.default, var.security_group_ids.users]
  idle_timeout    = var.loadbalance_external_timeout
  tags = {
    Env = var.env
  }
}

resource "aws_alb_listener" "public_http" {
  load_balancer_arn = aws_alb.public.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

data "aws_acm_certificate" "default" {
  domain   = "*.media.its.manchester.ac.uk"
  statuses = ["ISSUED"]
}

resource "aws_alb_listener" "public_https" {
  load_balancer_arn = aws_alb.public.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = var.loadbalance_security_policy
  certificate_arn   = data.aws_acm_certificate.default.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.video.arn
  }
}

resource "aws_wafv2_web_acl_association" "public" {
  resource_arn = aws_alb.public.arn
  web_acl_arn = var.waf_public_arn
}

# Restricted,
resource "aws_alb" "restricted" {
  name     = "${var.prefix}-restricted"
  internal = false
  subnets         = var.subnet_ids.restricted
  security_groups = [var.security_group_ids.default, var.security_group_ids.restricted]
  idle_timeout    = var.loadbalance_external_timeout
  tags = {
    Env = var.env
  }
}

resource "aws_alb_listener" "restricted_http" {
  load_balancer_arn = aws_alb.restricted.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "restricted_https" {
  load_balancer_arn = aws_alb.restricted.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = var.loadbalance_security_policy
  certificate_arn   = data.aws_acm_certificate.default.arn
  default_action {
    target_group_arn = aws_alb_target_group.upload.arn
    type             = "forward"
  }
}

#
# Target Groups
#

# Admin
resource "aws_alb_listener_certificate" "admin" {
  listener_arn = aws_alb_listener.restricted_https.arn
  certificate_arn = var.certificate_arns.admin
}

resource "aws_alb_target_group" "admin" {
  name     = "${var.prefix}-admin"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.opencast_health_check_path
  }
  tags = {
    Env       = var.env
    App       = "opencast"
    Component = "administration"
  }
}

resource "aws_alb_target_group_attachment" "admin" {
  count            = var.admin_standby ? 2 : 1
  target_group_arn = aws_alb_target_group.admin.arn
  target_id        = aws_instance.admin[count.index].id
}

resource "aws_alb_listener_rule" "admin" {
  depends_on   = [aws_alb_target_group.admin]
  listener_arn = aws_alb_listener.restricted_https.arn
  priority     = 10
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.admin.arn
  }
  condition {
    host_header {
      values = ["admin.*"]
    }
  }
}

# Edit
resource "aws_alb_listener_certificate" "edit" {
  listener_arn = aws_alb_listener.public_https.arn
  certificate_arn = var.certificate_arns.edit
}

resource "aws_alb_target_group" "edit" {
  name     = "${var.prefix}-edit"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.opencast_health_check_path

    healthy_threshold   = var.loadbalance_healthy_threshold_slow
    unhealthy_threshold = var.loadbalance_unhealthy_threshold
    interval            = var.loadbalance_health_interval
    timeout             = var.loadbalance_health_timeout
  }
  stickiness {
    type    = "lb_cookie"
    enabled = true
  }
  tags = {
    Env       = var.env
    App       = "opencast"
    Component = "editing"
  }
}

resource "aws_alb_target_group_attachment" "edit" {
  count            = var.edit_count
  target_group_arn = aws_alb_target_group.edit.arn
  target_id        = aws_instance.edit[count.index].id
}

resource "aws_alb_listener_rule" "edit" {
  depends_on   = [aws_alb_target_group.edit]
  listener_arn = aws_alb_listener.public_https.arn
  priority     = 20
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.edit.arn
  }
  condition {
    host_header {
      values = ["edit.*"]
    }
  }
}

# Upload
resource "aws_alb_listener_certificate" "upload" {
  listener_arn = aws_alb_listener.restricted_https.arn
  certificate_arn = var.certificate_arns.upload
}

# Target Group is attached to an autoscaling group
resource "aws_alb_target_group" "upload" {
  name     = "${var.prefix}-upload"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.opencast_health_check_path

    healthy_threshold   = var.loadbalance_healthy_threshold_slow
    unhealthy_threshold = var.loadbalance_unhealthy_threshold
    interval            = var.loadbalance_health_interval
    timeout             = var.loadbalance_health_timeout
  }
  stickiness {
    type    = "lb_cookie"
    enabled = true
  }
  tags = {
    Env       = var.env
    App       = "opencast"
    Component = "ingest"
  }
}

resource "aws_alb_listener_rule" "upload" {
  depends_on   = [aws_alb_target_group.upload]
  listener_arn = aws_alb_listener.restricted_https.arn
  priority     = 20
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.upload.arn
  }
  condition {
    host_header {
      values = ["upload.*"]
    }
  }
}

# Log
resource "aws_alb_listener_certificate" "log" {
  listener_arn = aws_alb_listener.restricted_https.arn
  certificate_arn = var.certificate_arns.log
}

# Target Group is attached to an autoscaling group
resource "aws_alb_target_group" "log" {
  name     = "${var.prefix}-log"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = "/"
  }
  tags = {
    Env       = var.env
    App       = "graylog"
    Component = "logging"
  }
}

resource "aws_alb_target_group_attachment" "log" {
  target_group_arn = aws_alb_target_group.log.arn
  target_id        = aws_instance.log.id
}

resource "aws_alb_listener_rule" "log" {
  depends_on = [
    aws_alb_target_group.log]
  listener_arn = aws_alb_listener.restricted_https.arn
  priority = 30
  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.log.arn
  }
  condition {
    host_header {
      values = ["log.*"]
    }
  }
}

# Video
resource "aws_alb_listener_certificate" "video" {
  listener_arn = aws_alb_listener.public_https.arn
  certificate_arn = var.certificate_arns.video
}

resource "aws_alb_target_group" "video" {
  name     = "${var.prefix}-video"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.video_health_check_path

    healthy_threshold   = var.loadbalance_healthy_threshold
    unhealthy_threshold = var.loadbalance_unhealthy_threshold
    interval            = var.loadbalance_health_interval
    timeout             = var.loadbalance_health_timeout
  }
  stickiness {
    type    = "lb_cookie"
    enabled = true
  }
  tags = {
    Env       = var.env
    App       = "videolounge"
    Component = "video"
  }
}

resource "aws_alb_target_group_attachment" "video" {
  count            = var.video_count
  target_group_arn = aws_alb_target_group.video.arn
  target_id        = aws_instance.video[count.index].id
}

resource "aws_alb_listener_rule" "video" {
  depends_on   = [aws_alb_target_group.video]
  listener_arn = aws_alb_listener.public_https.arn
  priority     = 10
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.video.arn
  }
  condition {
    host_header {
      values = ["video.*"]
    }
  }
}

# Tools
resource "aws_alb_listener_certificate" "tools" {
  listener_arn = aws_alb_listener.public_https.arn
  certificate_arn = var.certificate_arns.tools
}

resource "aws_alb_target_group" "tools" {
  name     = "${var.prefix}-tools"
  vpc_id   = var.vpc_id
  port     = 80
  protocol = "HTTP"
  health_check {
    protocol = "HTTP"
    path     = local.tools_health_check_path

    healthy_threshold   = var.loadbalance_healthy_threshold
    unhealthy_threshold = var.loadbalance_unhealthy_threshold
    interval            = var.loadbalance_health_interval
    timeout             = var.loadbalance_health_timeout
  }
  stickiness {
    type    = "lb_cookie"
    enabled = true
  }
  tags = {
    Env       = var.env
    App       = "tools"
    Component = "tools"
  }
}

resource "aws_alb_target_group_attachment" "tools" {
  count            = var.tools_count
  target_group_arn = aws_alb_target_group.tools.arn
  target_id        = aws_instance.tools[count.index].id
}

resource "aws_alb_listener_rule" "tools" {
  depends_on = [
    aws_alb_target_group.tools]
  listener_arn = aws_alb_listener.public_https.arn
  priority = 40
  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.tools.arn
  }
  condition {
    host_header {
      values = ["tools.*"]
    }
  }
}
