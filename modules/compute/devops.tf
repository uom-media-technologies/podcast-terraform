# compute/devops.tf

#
# operations
#
resource "aws_instance" "ops" {
  ami           = data.aws_ami.podcast.id
  instance_type = var.ops_type
  subnet_id     = element(var.subnet_ids.restricted, var.primary_zone_index)
  key_name      = local.key_bastion_name
  root_block_device {
    delete_on_termination = true
  }
  associate_public_ip_address = true
  vpc_security_group_ids      = [var.security_group_ids.default, var.security_group_ids.bastion]
  iam_instance_profile        = var.profile_names.ops
  user_data                   = local.script_operations
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    // cloud-init only used for instance bootstrapping
    ignore_changes = [ ami, user_data ]
  }
  tags = {
    Name = "${var.prefix}-ops"
    Env  = var.env
    App  = "ansible"
    host = "operations"
  }
}

#
# logging
#
resource "aws_instance" "log" {
  ami           = data.aws_ami.podcast.id
  instance_type = var.log_type
  subnet_id     = element(var.subnet_ids.processing, var.primary_zone_index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
    volume_size = var.log_volume_size
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  lifecycle {
    ignore_changes = [ami]
  }
  tags = {
    Name = "${var.prefix}-log"
    Env  = var.env
    App  = "graylog"
    host = "logging"
  }
}

