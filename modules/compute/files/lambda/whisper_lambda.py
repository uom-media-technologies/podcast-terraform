import json
import boto3
import requests
from requests.auth import HTTPDigestAuth
import os

def lambda_handler(event, context):
    # Configuration
    autoscaling_group_name = os.environ['AUTOSCALING_GROUP']
    api_url = os.environ['API_URL']
    username = os.environ['OPENCAST_USER']
    password = os.environ['OPENCAST_PASSWORD']
    max_queue_size = int(os.environ['MAX_QUEUE_SIZE'])
    jobs_threshold = int(os.environ['SCALE_IN_JOBS_THRESHOLD'])
    slack_webhook_url = os.getenv('SLACK_WEBHOOK_URL')

    custom_header = {'X-Requested-Auth': 'Digest', 'X-Opencast-Matterhorn-Authorization': 'true'}
    jobType_whisper = 'org.opencastproject.speechtotext'
    jobType_workflow = 'org.opencastproject.workflow'

    number_of_queued_whisper_jobs = 0
    number_of_running_whisper_jobs = 0
    running_workflow = False

    # Default slack message
    slack_message = {
        "text": f"{autoscaling_group_name}\nERROR! Lambda function unable to retrieve active jobs. Fix issue urgently to avoid job queues.",
    }

    # Create boto3 clients
    autoscaling_client = boto3.client('autoscaling')

    # Call the API with Digest authentication and custom header
    response = requests.get(api_url, auth=HTTPDigestAuth(username, password), headers=custom_header)
    if response.status_code != 200:
        send_slack_message(slack_message, slack_webhook_url)
        raise Exception(f"API call failed with status code {response.status_code}, the response is:\n{response.text}")

    if response.content:
        activeJob_json = json.loads(response.content)
        if 'jobs' in activeJob_json and 'job' in activeJob_json['jobs']:
            activeJob_arr = activeJob_json['jobs']['job']
            # Ensure activeJob_arr is a list
            if not isinstance(activeJob_arr, list):
                activeJob_arr = [activeJob_arr]
            
            for job in activeJob_arr:
                if isinstance(job, dict):
                    if 'type' in job and job['type'] == jobType_whisper:
                        if job.get('status') == 'QUEUED':
                            number_of_queued_whisper_jobs += 1
                        elif job.get('status') == 'RUNNING':
                            number_of_running_whisper_jobs += 1
                    if not running_workflow:
                        if 'type' in job and job['type'] == jobType_workflow and job.get('status') == 'RUNNING':
                            running_workflow = True
                else:
                    print("Invalid job format:", job)
        else:
            print("No jobs found in the response.")
    else:
        send_slack_message(slack_message, slack_webhook_url)
        raise Exception("Failed to get active jobs")

    # Get current desired capacity
    asg_info = autoscaling_client.describe_auto_scaling_groups(
        AutoScalingGroupNames=[autoscaling_group_name]
    )

    if len(asg_info['AutoScalingGroups']) == 0:
        slack_message = {
            "text": f"{autoscaling_group_name}\nERROR! Lambda function unable to find autoscaling group: {autoscaling_group_name}. Fix issue urgently to avoid job queues.",
        }
        send_slack_message(slack_message, slack_webhook_url)
        raise Exception(f"Auto Scaling group '{autoscaling_group_name}' not found")

    max_gpu = asg_info['AutoScalingGroups'][0]['MaxSize']
    min_gpu = asg_info['AutoScalingGroups'][0]['MinSize']
    current_capacity = asg_info['AutoScalingGroups'][0]['DesiredCapacity']

    # Determine new capacity based on queued/running jobs
    new_capacity = current_capacity
    if current_capacity == 0 and running_workflow:
        new_capacity = min(current_capacity + 1, max_gpu)
    elif number_of_queued_whisper_jobs >= max_queue_size:
        new_capacity = min(current_capacity + 1, max_gpu)
    elif number_of_queued_whisper_jobs == 0 and 0 < number_of_running_whisper_jobs < jobs_threshold:
        new_capacity = max(current_capacity - 1, 1)
    elif number_of_queued_whisper_jobs == 0 and number_of_running_whisper_jobs == 0 and running_workflow is False:
        new_capacity = max(current_capacity - 1, min_gpu)

    # Update the Auto Scaling group if capacity changed
    if new_capacity != current_capacity:
        try: 
            autoscaling_client.update_auto_scaling_group(
                AutoScalingGroupName=autoscaling_group_name,
                DesiredCapacity=new_capacity
            )
        except:
            slack_message = {
                "text": f"{autoscaling_group_name}\nERROR! Lambda function unable to update autoscaling group: {autoscaling_group_name}. Fix issue urgently to avoid job queues.",
            }
            send_slack_message(slack_message, slack_webhook_url)
            print(f"Error - Unable to update autoscaling group: {autoscaling_group_name}")

    output = json.dumps({
        'statusCode': response.status_code,
        'message': 'Auto Scaling group updated successfully',
        'minimum capacity': min_gpu,
        'maximum capacity': max_gpu,
        'current_capacity': new_capacity,
        'number of running whisper jobs': number_of_running_whisper_jobs,
        'number of queued whisper jobs': number_of_queued_whisper_jobs,
        'workflow running': running_workflow
    })

    # Useful for showing output in AWS Cloudwatch logs    
    print(output)

    return {
        'body': output
    }

def send_slack_message(message, url):
    try: 
        response = requests.post(
            url,
            data=json.dumps(message),
            headers={'Content-Type': 'application/json'}
        )
    except Exception as e:
        print(f"Error - slack message not sent: {e}")
