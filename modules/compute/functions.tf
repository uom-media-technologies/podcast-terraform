#
# Lambda function for whisper
#

data "aws_secretsmanager_secret_version" "opencast" {
  secret_id = "media/podcasting/${var.env}/webservice/opencast/rest"
}

data "aws_secretsmanager_secret_version" "slack" {
  secret_id = "media/podcasting/live/webservice/slack/lambda"
}

# TODO investigate option to move policy to roles.tf
# Policies added here because aws_lambda_function needs a role instead of a role profile

# Whisper lambda policy
resource "aws_iam_policy" "lambda_whisper" {
  name        = "${var.prefix}-lambda-whisper"
  description = "Allow lambda to interact with other resources and network"
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CreateGroup",
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:*:600755374209:*"
        },
        {
            "Sid": "CreateLog",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:600755374209:log-group:/aws/lambda/*:*"
        },
        {
            "Sid": "UpdateNetwork",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeNetworkInterfaces",
                "ec2:CreateNetworkInterface",
                "ec2:DeleteNetworkInterface",
                "ec2:DescribeInstances",
                "ec2:AttachNetworkInterface"
            ],
            "Resource": "*"
        },
        {
            "Sid": "ReadInstanceLifcycle",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeLifecycleHooks"
            ],
            "Resource": "*"
        },
        {
            "Sid": "UpdateLifcycle",
            "Effect": "Allow",
            "Action": [
                "autoscaling:CompleteLifecycleAction",
                "autoscaling:UpdateAutoScalingGroup"
            ],
            "Resource": "arn:aws:autoscaling:*:600755374209:autoScalingGroup:*:autoScalingGroupName/*"
        }
    ]
}
EOF

}

resource "aws_iam_role" "whisper_lambda" {
  name               = "${var.prefix}-lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      }
    }
  ]
}
EOF

  tags = {
    Env = var.env
  }
}

data "aws_iam_policy" "lambda_vpc_access" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda_vpc" {
  role       = aws_iam_role.whisper_lambda.name
  policy_arn = data.aws_iam_policy.lambda_vpc_access.arn
}

resource "aws_iam_role_policy_attachment" "lambda_log" {
  role       = aws_iam_role.whisper_lambda.name
  policy_arn = aws_iam_policy.lambda_whisper.arn
}

resource "null_resource" "whisper_lambda_package" {
  triggers = {
    function_hash =  filebase64sha256("${path.module}/files/lambda/whisper_lambda.py")
  }
  provisioner "local-exec" {
    # Update the package with the latest script
    command = "zip whisper-autoscaling-package.zip whisper_lambda.py"
    working_dir = "${path.module}/files/lambda"
  }
}

resource "aws_lambda_function" "whisper_lambda_function" {
  filename                = "${path.module}/files/lambda/whisper-autoscaling-package.zip"
  function_name           = "${var.prefix}-trigger-whisper-autoscaling"
  role                    =  aws_iam_role.whisper_lambda.arn
  handler                 = "whisper_lambda.lambda_handler"
  runtime                 = "python3.9"
  timeout                 =  300
  source_code_hash = filebase64sha256("${path.module}/files/lambda/whisper-autoscaling-package.zip")

  vpc_config {
    subnet_ids            =  var.subnet_ids.delivery
    security_group_ids    = [var.security_group_ids.default]
  }

  environment {
    variables = {
      API_URL                  = "${var.opencast_server}/services/activeJobs.json"
      OPENCAST_USER            = jsondecode(data.aws_secretsmanager_secret_version.opencast.secret_string)["login"]
      OPENCAST_PASSWORD        = jsondecode(data.aws_secretsmanager_secret_version.opencast.secret_string)["password"]
      AUTOSCALING_GROUP        = "${var.prefix}-whisper"
      MAX_QUEUE_SIZE           = var.whisper_job_queue_size
      SCALE_IN_JOBS_THRESHOLD  = var.whisper_scale_in_job_threshold
      SLACK_WEBHOOK_URL        = jsondecode(data.aws_secretsmanager_secret_version.slack.secret_string)["url"]
    }
  }

  tags = {
    Env = var.env
  }
}

# Cloudcatch set up to run whisper lambda function
resource "aws_lambda_permission" "allow_cloudwatch" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.whisper_lambda_function.function_name
    principal = "events.amazonaws.com"
    source_arn = aws_cloudwatch_event_rule.whisper-lambda.arn

}

resource "aws_cloudwatch_event_rule" "whisper-lambda" {
  name                  = "${var.prefix}-run-whisper-lambda-function"
  description           = "Schedule whisper lambda function run"
  schedule_expression   = "rate(${var.whisper_lambda_call_rate})"
  tags = {
    Env = var.env
  }
}

resource "aws_cloudwatch_event_target" "whisper-lambda-function-target" {
  target_id = "whisper-lambda-function-target"
  rule      = aws_cloudwatch_event_rule.whisper-lambda.name
  arn       = aws_lambda_function.whisper_lambda_function.arn
}
