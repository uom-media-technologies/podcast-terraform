/**
 * # Compute
 *
 * Define all instances, target groups and loadbalancers. ECS types and target groups sizes defined as locals,
 * see compute/main.tf
 *
 * ## Usage:
 * ```
 * module "compute" {
 *   source = "./compute"
 *   prefix = "podcast-prod"
 *   env = "prod"
 *   region = "us-east"
 *   zones = ["us-east-1", "us-west-1"]
 *   account = "12345678901"
 *   key_name = "podcast"
 *   protocol_external = "${local.protocol_external}"
 *
 *   vpc_id = "${module.network.vpc_id}"
 *   subnet_public_ids = "${module.network.subnet_public_ids}"
 *   subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
 *   subnet_processing_ids = "${module.network.subnet_processing_ids}"
 *   subnet_delivery_ids = "${module.network.subnet_delivery_ids}"
 *
 *   security_group_default_id = "${module.security.security_group_default_id}"
 *   security_group_users_id = "${module.security.security_group_users_id}"
 *   security_group_operations_id = "${module.security.security_group_operations_id}"
 *   profile_publication_name = "${module.security.profile_publication_name}"
 *   profile_admin_name = "${module.security.profile_admin_name}"
 *
 *   efs_share_points = "${module.storage.efs_share_points}"
 *   ebs_opencast_index_id = "${module.storage.ebs_opencast_index_id}"
 *   s3_archive_id = "${module.storage.s3_archive_id}"
 *   s3_distribution_id = "${module.storage.s3_distribution_id}"* }

 *   admin_type =  "t2.large"
 *   edit_type = "t2.medium"
 *   upload_type = "t2.medium"
 *   work_type ="c5.xlarge"

 *   tools_type = "t2.small"
 *   video_type = "t2.small"
 *   ops_type = "t2.micro"
 *   build_type = "t2.medium"
 *   log_type = "t2.small"
 *
 *   admin_standby = true
 *   tools_standby = true
 *   edit_count = 2
 *   video_count = 2
 *
 *   autoscale_upload_max = 2
 *   autoscale_upload_min = 1
 *   autoscale_upload_desired = 2
 *   autoscale_upload_quiet_max = 1
 *   autoscale_upload_quiet_min = 1
 *   autoscale_upload_quiet_desired = 1
 *   autoscale_upload_network_in_target = 10000000 # bytes
 *
 *   autoscale_work_max = 4
 *   autoscale_work_min = 2
 *   autoscale_work_desired = 3
 *   autoscale_work_quiet_max = 2
 *   autoscale_work_quiet_min = 1
 *   autoscale_work_quiet_desired = 2
 *   autoscale_work_cpu_target = 70.0 # %
 * ```
 */

# nfs mount options for efs
locals {
  nfs_options = "nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport,_netdev"
}

locals {
  opencast_health_check_path     = "/info/health"
  video_health_check_path        = "/weblounge-health"
  tools_health_check_path        = "/index.html"
}

locals {
  autoscale_work_heartbeat   = 600 # secs
  autoscale_upload_heartbeat = 600 # secs
  autoscale_whisper_terminate_heartbeat = 300 # secs
  autoscale_whisper_launch_heartbeat = 300 # secs
  autoscale_timezone         = "Europe/London"
}

locals {
  template_script_primary               = "${path.module}/scripts/primary"
  template_script_standby               = "${path.module}/scripts/standby"
  template_script_operations            = "${path.module}/scripts/operations"
  template_script_fragment_mount_efs    = "${path.module}/scripts/fragment_mount_efs"
  template_script_fragment_opencast_config = "${path.module}/scripts/fragment_opencast_config"
  template_script_fragment_opencast_autoscale_config = "${path.module}/scripts/fragment_opencast_autoscale_config"
}

locals {
  key_access_name  = "${var.prefix}-access"
  key_bastion_name = "${var.prefix}-bastion"
}

locals {
  environment_names = {
    dev = "Development"
    stage = "Staging"
    prod = "Production"
  }
}

# Templates for cloud-init scripts
locals {
  script_operations = templatefile(local.template_script_operations, {
    ec2_user    = "ec2-user"
    vpc_pattern = replace(var.vpc_cidr, "/\\.0\\/[0-9]*/", ".*" )
    region      = var.region
    })
  script_fragment_mount_efs = templatefile(local.template_script_fragment_mount_efs, {
    zone0         = var.zones[0]
    mount_point   = "/var/opencast"
    mount_target0 = element(var.efs_share_points, 0)
    mount_target1 = element(var.efs_share_points, 1)
    mount_options = local.nfs_options
    })
  script_fragment_opencast_config = templatefile(local.template_script_fragment_opencast_config, {})
  script_fragment_opencast_autoscale_config = templatefile(local.template_script_fragment_opencast_autoscale_config, {
    environment_name = local.environment_names[var.env]
    })
  script_primary = templatefile(local.template_script_primary, {
    fragment_mount_efs    = ""
    fragment_opencast_config = ""
  })
  script_standby = templatefile(local.template_script_standby, {
    fragment_mount_efs    = ""
    fragment_opencast_config = ""
  })
  # FIXME: fragment_opencast_config should be empty as Admin, Edit are not built from an opencast AMI
  # changing the fragment will cause the instance to be recreated
  script_opencast_primary = templatefile(local.template_script_primary, {
    fragment_mount_efs    = local.script_fragment_mount_efs
    fragment_opencast_config = local.script_fragment_opencast_config
  })
  script_opencast_standby = templatefile(local.template_script_standby, {
    fragment_mount_efs    = local.script_fragment_mount_efs
    fragment_opencast_config = local.script_fragment_opencast_config
  })
  script_opencast_autoscale = templatefile(local.template_script_primary, {
    fragment_mount_efs    = local.script_fragment_mount_efs
    fragment_opencast_config = local.script_fragment_opencast_autoscale_config
  })
}
