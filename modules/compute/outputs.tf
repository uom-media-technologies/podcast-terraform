# compute/outputs.tf

output "lb_public_https_arn" {
  value = aws_alb_listener.public_https.arn
}

output "lb_public_dns_name" {
  value = aws_alb.public.dns_name
}

output "lb_public_zone_id" {
  value = aws_alb.public.zone_id
}

output "lb_restricted_https_arn" {
  value = aws_alb_listener.restricted_https.arn
}

output "lb_restricted_dns_name" {
  value = aws_alb.restricted.dns_name
}

output "lb_private_dns_name" {
  value = aws_alb.private.dns_name
}

output "lb_restricted_zone_id" {
  value = aws_alb.restricted.zone_id
}

output "lb_private_zone_id" {
  value = aws_alb.private.zone_id
}

output "ops_ip" {
  value = aws_instance.ops.public_ip == "" ? aws_instance.ops.private_ip : aws_instance.ops.public_ip
}

output "ops_ip_internal" {
  value = aws_instance.ops.private_ip
}

output "log_ip" {
  value = aws_instance.log.public_ip == "" ? aws_instance.log.private_ip : aws_instance.log.public_ip
}

output "log_ip_internal" {
  value = aws_instance.log.private_ip
}

output "admin_ips" {
  value = aws_instance.admin.*.private_ip
}

output "index_ips" {
  value = aws_instance.index.*.private_ip
}

output "admin_ids" {
  value = aws_instance.admin.*.id
}
