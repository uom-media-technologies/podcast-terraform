# Compute

Define all instances, target groups and loadbalancers. ECS types and target groups sizes defined as locals,
see compute/main.tf

## Usage:
```
module "compute" {
  source = "./compute"
  prefix = "podcast-prod"
  env = "prod"
  region = "us-east"
  zones = ["us-east-1", "us-west-1"]
  account = "12345678901"
  key_name = "podcast"
  protocol_external = "${local.protocol_external}"

  vpc_id = "${module.network.vpc_id}"
  subnet_public_ids = "${module.network.subnet_public_ids}"
  subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
  subnet_processing_ids = "${module.network.subnet_processing_ids}"
  subnet_delivery_ids = "${module.network.subnet_delivery_ids}"

  security_group_default_id = "${module.security.security_group_default_id}"
  security_group_users_id = "${module.security.security_group_users_id}"
  security_group_operations_id = "${module.security.security_group_operations_id}"
  profile_publication_name = "${module.security.profile_publication_name}"
  profile_admin_name = "${module.security.profile_admin_name}"

  efs_share_points = "${module.storage.efs_share_points}"
  ebs_opencast_index_id = "${module.storage.ebs_opencast_index_id}"
  s3_archive_id = "${module.storage.s3_archive_id}"
  s3_distribution_id = "${module.storage.s3_distribution_id}"* }
```

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/acm_certificate) | data source |
| [aws_ami.podcast](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_ami.upload](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_ami.whisper](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_ami.worker](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_iam_policy.lambda_vpc_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_secretsmanager_secret_version.opencast](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_secretsmanager_secret_version.slack](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_admin_standby"></a> [admin\_standby](#input\_admin\_standby) | Create a standby Opencast Admin instance | `bool` | `false` | no |
| <a name="input_admin_type"></a> [admin\_type](#input\_admin\_type) | Opencast Admin instance type | `string` | `"t3.large"` | no |
| <a name="input_autoscale_upload_desired"></a> [autoscale\_upload\_desired](#input\_autoscale\_upload\_desired) | Desired number of Opencast Upload instances during active periods | `number` | `1` | no |
| <a name="input_autoscale_upload_max"></a> [autoscale\_upload\_max](#input\_autoscale\_upload\_max) | Maximum number of Opencast Upload instances during active periods | `number` | `2` | no |
| <a name="input_autoscale_upload_min"></a> [autoscale\_upload\_min](#input\_autoscale\_upload\_min) | Minimum number of Opencast Upload instances during active periods | `number` | `1` | no |
| <a name="input_autoscale_upload_network_in_target"></a> [autoscale\_upload\_network\_in\_target](#input\_autoscale\_upload\_network\_in\_target) | Opencast Upload Target network load B/s | `number` | `10000000` | no |
| <a name="input_autoscale_upload_quiet_desired"></a> [autoscale\_upload\_quiet\_desired](#input\_autoscale\_upload\_quiet\_desired) | Desired number of Opencast Upload instances during quiet periods | `number` | `1` | no |
| <a name="input_autoscale_upload_quiet_max"></a> [autoscale\_upload\_quiet\_max](#input\_autoscale\_upload\_quiet\_max) | Maximum number of Opencast Upload instances during quiet periods | `number` | `1` | no |
| <a name="input_autoscale_upload_quiet_min"></a> [autoscale\_upload\_quiet\_min](#input\_autoscale\_upload\_quiet\_min) | Minimum number of Opencast Upload instances during quiet periods | `number` | `1` | no |
| <a name="input_autoscale_whisper_desired"></a> [autoscale\_whisper\_desired](#input\_autoscale\_whisper\_desired) | Desired number of Opencast Whisper instances during active periods | `number` | `0` | no |
| <a name="input_autoscale_whisper_max"></a> [autoscale\_whisper\_max](#input\_autoscale\_whisper\_max) | Maximum number of Opencast Whisper instances during active periods | `number` | `1` | no |
| <a name="input_autoscale_whisper_min"></a> [autoscale\_whisper\_min](#input\_autoscale\_whisper\_min) | Minimum number of Opencast Whisper instances during active periods | `number` | `0` | no |
| <a name="input_autoscale_whisper_warm_max"></a> [autoscale\_whisper\_warm\_max](#input\_autoscale\_whisper\_warm\_max) | Maximum number of Opencast Whisper warm instances | `number` | `1` | no |
| <a name="input_autoscale_whisper_warm_min"></a> [autoscale\_whisper\_warm\_min](#input\_autoscale\_whisper\_warm\_min) | Minimum number of Opencast Whisper warm instances | `number` | `0` | no |
| <a name="input_autoscale_work_cpu_target"></a> [autoscale\_work\_cpu\_target](#input\_autoscale\_work\_cpu\_target) | Opencast Work instance target CPU load % | `number` | `70` | no |
| <a name="input_autoscale_work_desired"></a> [autoscale\_work\_desired](#input\_autoscale\_work\_desired) | Desired number of Opencast Work instances during active periods | `number` | `1` | no |
| <a name="input_autoscale_work_max"></a> [autoscale\_work\_max](#input\_autoscale\_work\_max) | Maximum number of Opencast Work instances during active periods | `number` | `2` | no |
| <a name="input_autoscale_work_min"></a> [autoscale\_work\_min](#input\_autoscale\_work\_min) | Minimum number of Opencast Work instances during active periods | `number` | `1` | no |
| <a name="input_autoscale_work_quiet_desired"></a> [autoscale\_work\_quiet\_desired](#input\_autoscale\_work\_quiet\_desired) | Desired number of Opencast Work instances during quiet periods | `number` | `1` | no |
| <a name="input_autoscale_work_quiet_max"></a> [autoscale\_work\_quiet\_max](#input\_autoscale\_work\_quiet\_max) | Maximum number of Opencast Work instances during quiet periods | `number` | `1` | no |
| <a name="input_autoscale_work_quiet_min"></a> [autoscale\_work\_quiet\_min](#input\_autoscale\_work\_quiet\_min) | Minimum number of Opencast Work instances during quiet periods | `number` | `1` | no |
| <a name="input_certificate_arns"></a> [certificate\_arns](#input\_certificate\_arns) | ARNs of SSL certificates | <pre>object({<br>    admin  = string<br>    upload = string<br>    edit   = string<br>    video  = string<br>    tools  = string<br>    log    = string<br>  })</pre> | n/a | yes |
| <a name="input_ebs_opencast_index_id"></a> [ebs\_opencast\_index\_id](#input\_ebs\_opencast\_index\_id) | storage inputs | `any` | n/a | yes |
| <a name="input_ebs_video_index_ids"></a> [ebs\_video\_index\_ids](#input\_ebs\_video\_index\_ids) | n/a | `list(string)` | n/a | yes |
| <a name="input_edit_count"></a> [edit\_count](#input\_edit\_count) | Number of Opencast Edit instances (static) | `number` | `1` | no |
| <a name="input_edit_type"></a> [edit\_type](#input\_edit\_type) | Opencast Edit instance type | `string` | `"t3.small"` | no |
| <a name="input_efs_share_points"></a> [efs\_share\_points](#input\_efs\_share\_points) | n/a | `list(string)` | n/a | yes |
| <a name="input_env"></a> [env](#input\_env) | Deployed environment | `any` | n/a | yes |
| <a name="input_index_type"></a> [index\_type](#input\_index\_type) | Elasticsearch index instance type | `string` | `"t3.medium"` | no |
| <a name="input_index_volume_size"></a> [index\_volume\_size](#input\_index\_volume\_size) | Size of index instances root volume | `number` | `5` | no |
| <a name="input_loadbalance_edit_internal_stickiness"></a> [loadbalance\_edit\_internal\_stickiness](#input\_loadbalance\_edit\_internal\_stickiness) | Stickiness duration for Opencast Edit internal load-balancing specifically OAIPMH harvesting | `number` | `3600` | no |
| <a name="input_loadbalance_external_timeout"></a> [loadbalance\_external\_timeout](#input\_loadbalance\_external\_timeout) | Number of seconds to wait before closing and idle connection | `number` | `60` | no |
| <a name="input_loadbalance_health_interval"></a> [loadbalance\_health\_interval](#input\_loadbalance\_health\_interval) | Number seconds between health checks | `number` | `10` | no |
| <a name="input_loadbalance_health_timeout"></a> [loadbalance\_health\_timeout](#input\_loadbalance\_health\_timeout) | Number of seconds to wait before considering the health check to fail | `number` | `5` | no |
| <a name="input_loadbalance_healthy_threshold"></a> [loadbalance\_healthy\_threshold](#input\_loadbalance\_healthy\_threshold) | Number of valid responses for a target to become healthy (min 2) | `number` | `2` | no |
| <a name="input_loadbalance_healthy_threshold_slow"></a> [loadbalance\_healthy\_threshold\_slow](#input\_loadbalance\_healthy\_threshold\_slow) | Number of successful responses for a slow to start target to become healthy (min 2) | `number` | `6` | no |
| <a name="input_loadbalance_security_policy"></a> [loadbalance\_security\_policy](#input\_loadbalance\_security\_policy) | Security Policy for HTTPS on the loadbalancer | `string` | `"ELBSecurityPolicy-TLS-1-2-2017-01"` | no |
| <a name="input_loadbalance_unhealthy_threshold"></a> [loadbalance\_unhealthy\_threshold](#input\_loadbalance\_unhealthy\_threshold) | Number of failed responses for a target to become unhealthy (min 2) | `number` | `2` | no |
| <a name="input_loadbalance_upload_internal_stickiness"></a> [loadbalance\_upload\_internal\_stickiness](#input\_loadbalance\_upload\_internal\_stickiness) | Stickiness duration for Opencast Upload internal load-balancing specifically tool uploads | `number` | `7200` | no |
| <a name="input_log_type"></a> [log\_type](#input\_log\_type) | Log instance type | `string` | `"t3.large"` | no |
| <a name="input_log_volume_size"></a> [log\_volume\_size](#input\_log\_volume\_size) | Size of tool instances root volume | `number` | `12` | no |
| <a name="input_opencast_password"></a> [opencast\_password](#input\_opencast\_password) | Password for Opencast REST API | `string` | `"CHANGE_ME"` | no |
| <a name="input_opencast_server"></a> [opencast\_server](#input\_opencast\_server) | Opencast server url | `string` | `"https://opencast.org"` | no |
| <a name="input_opencast_user"></a> [opencast\_user](#input\_opencast\_user) | User for Opencast REST API | `string` | `"opencast_system_account"` | no |
| <a name="input_opencast_volume_size"></a> [opencast\_volume\_size](#input\_opencast\_volume\_size) | Size of opencast instances root volume | `number` | `8` | no |
| <a name="input_ops_type"></a> [ops\_type](#input\_ops\_type) | Operation instance type | `string` | `"t3.micro"` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resource naming prefix | `any` | n/a | yes |
| <a name="input_primary_zone_index"></a> [primary\_zone\_index](#input\_primary\_zone\_index) | Primary availability zone | `number` | `0` | no |
| <a name="input_processing_dependences"></a> [processing\_dependences](#input\_processing\_dependences) | Resources that processing instances depend on | `any` | `[]` | no |
| <a name="input_profile_names"></a> [profile\_names](#input\_profile\_names) | instance profiles | <pre>object({<br>    admin  = string<br>    publication = string<br>    upload = string<br>    work   = string<br>    video  = string<br>    tools  = string<br>    ops    = string<br>    whisper = string<br>  })</pre> | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `any` | n/a | yes |
| <a name="input_s3_archive_id"></a> [s3\_archive\_id](#input\_s3\_archive\_id) | n/a | `any` | n/a | yes |
| <a name="input_s3_distribution_id"></a> [s3\_distribution\_id](#input\_s3\_distribution\_id) | n/a | `any` | n/a | yes |
| <a name="input_security_group_ids"></a> [security\_group\_ids](#input\_security\_group\_ids) | n/a | <pre>object({<br>    default = string<br>    users = string<br>    nat = string<br>    restricted = string<br>    bastion = string<br>  })</pre> | n/a | yes |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | n/a | <pre>object({<br>    public     = list(string)<br>    restricted = list(string)<br>    processing = list(string)<br>    delivery   = list(string)<br>  })</pre> | n/a | yes |
| <a name="input_tools_count"></a> [tools\_count](#input\_tools\_count) | Number of Podcast Tools instances (static) | `number` | `1` | no |
| <a name="input_tools_type"></a> [tools\_type](#input\_tools\_type) | Podcast tools instance type | `string` | `"t3.micro"` | no |
| <a name="input_tools_volume_size"></a> [tools\_volume\_size](#input\_tools\_volume\_size) | Size of tool instances root volume | `number` | `8` | no |
| <a name="input_upload_type"></a> [upload\_type](#input\_upload\_type) | Opencast Upload instance type | `string` | `"t3.small"` | no |
| <a name="input_video_count"></a> [video\_count](#input\_video\_count) | Number of Video Portal instances (static) | `number` | `1` | no |
| <a name="input_video_type"></a> [video\_type](#input\_video\_type) | Video Portal instance type | `string` | `"t3.small"` | no |
| <a name="input_video_volume_size"></a> [video\_volume\_size](#input\_video\_volume\_size) | Size of video instances root volume | `number` | `5` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC CIDR | `any` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC identity | `any` | n/a | yes |
| <a name="input_waf_public_arn"></a> [waf\_public\_arn](#input\_waf\_public\_arn) | ARN of the public Web Application Firewall | `any` | n/a | yes |
| <a name="input_whisper_gpu_type"></a> [whisper\_gpu\_type](#input\_whisper\_gpu\_type) | Opencast Whisper gpu instance type | `string` | `"g4dn.xlarge"` | no |
| <a name="input_whisper_job_queue_size"></a> [whisper\_job\_queue\_size](#input\_whisper\_job\_queue\_size) | Maximum number of whisper job to queue before adding new instance | `number` | `5` | no |
| <a name="input_whisper_lambda_call_rate"></a> [whisper\_lambda\_call\_rate](#input\_whisper\_lambda\_call\_rate) | set how often to execute whisper lambda function | `string` | `"5 minutes"` | no |
| <a name="input_whisper_scale_in_job_threshold"></a> [whisper\_scale\_in\_job\_threshold](#input\_whisper\_scale\_in\_job\_threshold) | Maximum number of whisper job to trigger scale in | `number` | `2` | no |
| <a name="input_work_type"></a> [work\_type](#input\_work\_type) | Opencast Work instance type | `string` | `"t3.medium"` | no |
| <a name="input_zones"></a> [zones](#input\_zones) | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_admin_ids"></a> [admin\_ids](#output\_admin\_ids) | n/a |
| <a name="output_admin_ips"></a> [admin\_ips](#output\_admin\_ips) | n/a |
| <a name="output_index_ips"></a> [index\_ips](#output\_index\_ips) | n/a |
| <a name="output_lb_private_dns_name"></a> [lb\_private\_dns\_name](#output\_lb\_private\_dns\_name) | n/a |
| <a name="output_lb_private_zone_id"></a> [lb\_private\_zone\_id](#output\_lb\_private\_zone\_id) | n/a |
| <a name="output_lb_public_dns_name"></a> [lb\_public\_dns\_name](#output\_lb\_public\_dns\_name) | n/a |
| <a name="output_lb_public_https_arn"></a> [lb\_public\_https\_arn](#output\_lb\_public\_https\_arn) | n/a |
| <a name="output_lb_public_zone_id"></a> [lb\_public\_zone\_id](#output\_lb\_public\_zone\_id) | n/a |
| <a name="output_lb_restricted_dns_name"></a> [lb\_restricted\_dns\_name](#output\_lb\_restricted\_dns\_name) | n/a |
| <a name="output_lb_restricted_https_arn"></a> [lb\_restricted\_https\_arn](#output\_lb\_restricted\_https\_arn) | n/a |
| <a name="output_lb_restricted_zone_id"></a> [lb\_restricted\_zone\_id](#output\_lb\_restricted\_zone\_id) | n/a |
| <a name="output_log_ip"></a> [log\_ip](#output\_log\_ip) | n/a |
| <a name="output_log_ip_internal"></a> [log\_ip\_internal](#output\_log\_ip\_internal) | n/a |
| <a name="output_ops_ip"></a> [ops\_ip](#output\_ops\_ip) | n/a |
| <a name="output_ops_ip_internal"></a> [ops\_ip\_internal](#output\_ops\_ip\_internal) | n/a |
