# compute/index.tf

#
# Elasticsearch Index
#
resource "aws_instance" "index" {
  count         = var.admin_standby ? 2 : 1
  ami           = data.aws_ami.podcast.id
  instance_type = var.index_type
  subnet_id     = element(var.subnet_ids.processing, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
    volume_size           = var.index_volume_size
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  user_data              = count.index == 0 ? local.script_primary : local.script_standby
  lifecycle {
    // cloud-init only used for instance bootstrapping
    ignore_changes = [ ami, user_data ]
  }
  tags = {
    Name = "${var.prefix}-index${count.index == 0 ? "" : "-standby"}"
    Env  = var.env
    App  = "elasticsearch"
    Component = "index"
    host = "index"
  }
}

resource "aws_volume_attachment" "index_index" {
  device_name = "/dev/sdf"
  volume_id   = var.ebs_opencast_index_id
  instance_id = aws_instance.index[var.primary_zone_index].id
}
