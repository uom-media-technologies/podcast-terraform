# compute/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

variable "primary_zone_index" {
  default     = 0
  description = "Primary availability zone"
}

variable "security_group_ids" {
  type = object({
    default = string
    users = string
    nat = string
    restricted = string
    bastion = string
  })
}

# instance profiles
variable "profile_names" {
  type = object({
    admin  = string
    publication = string
    upload = string
    work   = string
    video  = string
    tools  = string
    ops    = string
    whisper = string
  })
}

variable "certificate_arns" {
  type = object({
    admin  = string
    upload = string
    edit   = string
    video  = string
    tools  = string
    log    = string
  })
  description = "ARNs of SSL certificates"
}

# network inputs
variable "vpc_id" {
  description = "VPC identity"
}

variable "vpc_cidr" {
  description = "VPC CIDR"
}

variable "subnet_ids" {
  type = object({
    public     = list(string)
    restricted = list(string)
    processing = list(string)
    delivery   = list(string)
  })
}

# storage inputs
variable "ebs_opencast_index_id" {
}

variable "ebs_video_index_ids" {
  type = list(string)
}

variable "efs_share_points" {
  type = list(string)
}

variable "s3_archive_id" {
}

variable "s3_distribution_id" {
}

#
# configure instances
#

# instance type

variable "admin_type" {
  default     = "t3.large"
  description = "Opencast Admin instance type"
}

variable "edit_type" {
  default     = "t3.small"
  description = "Opencast Edit instance type"
}

variable "upload_type" {
  default     = "t3.small"
  description = "Opencast Upload instance type"
}

variable "work_type" {
  default     = "t3.medium"
  description = "Opencast Work instance type"
}

variable "index_type" {
  default     = "t3.medium"
  description = "Elasticsearch index instance type"
}

variable "tools_type" {
  default     = "t3.micro"
  description = "Podcast tools instance type"
}

variable "video_type" {
  default     = "t3.small"
  description = "Video Portal instance type"
}

variable "ops_type" {
  default     = "t3.micro"
  description = "Operation instance type"
}

variable "log_type" {
  default     = "t3.large"
  description = "Log instance type"
}

variable "whisper_gpu_type" {
  default     = "g4dn.xlarge"
  description = "Opencast Whisper gpu instance type"
}

# default node numbers
variable "admin_standby" {
  default     = false
  description = "Create a standby Opencast Admin instance"
}

variable "tools_count" {
  default     = 1
  description = "Number of Podcast Tools instances (static)"
}

variable "edit_count" {
  default     = 1
  description = "Number of Opencast Edit instances (static)"
}

variable "video_count" {
  default     = 1
  description = "Number of Video Portal instances (static)"
}

# instance storage
variable "opencast_volume_size" {
  default    = 8
  description = "Size of opencast instances root volume"
}

variable "log_volume_size" {
  default    = 12
  description = "Size of tool instances root volume"
}

variable "index_volume_size" {
  default    = 5
  description = "Size of index instances root volume"
}

variable "tools_volume_size" {
  default    = 8
  description = "Size of tool instances root volume"
}

variable "video_volume_size" {
  default    = 5
  description = "Size of video instances root volume"
}

# autoscaling
variable "autoscale_work_max" {
  default     = 2
  description = "Maximum number of Opencast Work instances during active periods"
}

variable "autoscale_work_min" {
  default     = 1
  description = "Minimum number of Opencast Work instances during active periods"
}

variable "autoscale_work_desired" {
  default     = 1
  description = "Desired number of Opencast Work instances during active periods"
}

variable "autoscale_work_quiet_max" {
  default     = 1
  description = "Maximum number of Opencast Work instances during quiet periods"
}

variable "autoscale_work_quiet_min" {
  default     = 1
  description = "Minimum number of Opencast Work instances during quiet periods"
}

variable "autoscale_work_quiet_desired" {
  default     = 1
  description = "Desired number of Opencast Work instances during quiet periods"
}

variable "autoscale_work_cpu_target" {
  default     = 70 # %
  description = "Opencast Work instance target CPU load %"
}

variable "autoscale_upload_max" {
  default     = 2
  description = "Maximum number of Opencast Upload instances during active periods"
}

variable "autoscale_upload_min" {
  default     = 1
  description = "Minimum number of Opencast Upload instances during active periods"
}

variable "autoscale_upload_desired" {
  default     = 1
  description = "Desired number of Opencast Upload instances during active periods"
}

variable "autoscale_upload_quiet_max" {
  default     = 1
  description = "Maximum number of Opencast Upload instances during quiet periods"
}

variable "autoscale_upload_quiet_min" {
  default     = 1
  description = "Minimum number of Opencast Upload instances during quiet periods"
}

variable "autoscale_upload_quiet_desired" {
  default     = 1
  description = "Desired number of Opencast Upload instances during quiet periods"
}

variable "autoscale_upload_network_in_target" {
  default     = 10000000 # bytes
  description = "Opencast Upload Target network load B/s"
}

# Auto scaling whisper
variable "whisper_lambda_call_rate" {
  default = "5 minutes"
  description = "set how often to execute whisper lambda function"
}

variable "whisper_job_queue_size" {
  default = 5
  description = "Maximum number of whisper job to queue before adding new instance"
}

variable "whisper_scale_in_job_threshold" {
  default = 2
  description = "Maximum number of whisper job to trigger scale in"
}

variable "autoscale_whisper_max" {
  default     = 1
  description = "Maximum number of Opencast Whisper instances during active periods"
}

variable "autoscale_whisper_min" {
  default     = 0
  description = "Minimum number of Opencast Whisper instances during active periods"
}

variable "autoscale_whisper_desired" {
  default     = 0
  description = "Desired number of Opencast Whisper instances during active periods"
}

variable "autoscale_whisper_warm_max" {
  default     = 1
  description = "Maximum number of Opencast Whisper warm instances"
}

variable "autoscale_whisper_warm_min" {
  default     = 0
  description = "Minimum number of Opencast Whisper warm instances"
}

variable "opencast_server" {
  default     = "https://opencast.org"
  description = "Opencast server url"
}

variable "opencast_user" {
  default     = "opencast_system_account"
  description = "User for Opencast REST API"
}

variable "opencast_password" {
  default     = "CHANGE_ME"
  description = "Password for Opencast REST API"
}

# Load Balancing
variable "loadbalance_security_policy" {
  default = "ELBSecurityPolicy-TLS-1-2-2017-01"
  description = "Security Policy for HTTPS on the loadbalancer"
}

variable "loadbalance_edit_internal_stickiness" {
  default = 3600
  description = "Stickiness duration for Opencast Edit internal load-balancing specifically OAIPMH harvesting"
}

variable "loadbalance_upload_internal_stickiness" {
  default = 7200
  description = "Stickiness duration for Opencast Upload internal load-balancing specifically tool uploads"
}

variable "loadbalance_healthy_threshold" {
  default = 2
  description = "Number of valid responses for a target to become healthy (min 2)"
}

variable "loadbalance_healthy_threshold_slow" {
  default = 6
  description = "Number of successful responses for a slow to start target to become healthy (min 2)"
}

variable "loadbalance_unhealthy_threshold" {
  default = 2
  description = "Number of failed responses for a target to become unhealthy (min 2)"
}

variable "loadbalance_health_interval" {
  default = 10
  description = "Number seconds between health checks"
}

variable "loadbalance_health_timeout" {
  default = 5
  description = "Number of seconds to wait before considering the health check to fail"
}

variable "loadbalance_external_timeout" {
  default = 60
  description = "Number of seconds to wait before closing and idle connection"
}

variable "waf_public_arn" {
  description = "ARN of the public Web Application Firewall"
}

variable "processing_dependences" {
  type = any
  default = []
  description = "Resources that processing instances depend on"
}

