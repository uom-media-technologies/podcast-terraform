# compute/image.tf

#
# AMIs for instances
#
data "aws_ami" "worker" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${var.prefix}-opencast-worker *"]
  }
  owners = ["self"]
}

data "aws_ami" "upload" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${var.prefix}-opencast-upload *"]
  }
  owners = ["self"]
}

data "aws_ami" "whisper" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${var.prefix}-opencast-whisper *"]
  }
  owners = ["self"]
}

# default AMI
data "aws_ami" "podcast" {
  most_recent = true
  filter {
   name   = "architecture"
   values = ["x86_64"]
  }
  filter {
   name   = "name"
   values = ["AlmaLinux OS 8*"]
  }
  owners = ["679593333241"]
}

locals {
  opencast_ami_id = data.aws_ami.podcast.id
  work_ami_id   = data.aws_ami.worker.image_id
  upload_ami_id = data.aws_ami.upload.image_id
  tools_ami_id = data.aws_ami.podcast.id
  video_ami_id = data.aws_ami.podcast.id
  whisper_ami_id = data.aws_ami.whisper.image_id
}


