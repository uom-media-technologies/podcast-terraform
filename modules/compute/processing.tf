# compute/processing.tf
# Opencast node instances

#
# Admin
#
resource "aws_instance" "admin" {
  count         = var.admin_standby ? 2 : 1
  ami           = local.opencast_ami_id
  instance_type = var.admin_type
  subnet_id     = element(var.subnet_ids.processing, count.index)
  key_name      = local.key_access_name
  root_block_device {
    volume_size = var.opencast_volume_size
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.admin
  user_data              = count.index == 0 ? local.script_opencast_primary : local.script_opencast_standby
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    // cloud-init only used for instance bootstrapping
    ignore_changes = [ ami, user_data ]
  }
  depends_on = [var.processing_dependences]
  tags = {
    Name      = "${var.prefix}-admin${count.index == 0 ? "" : "-standby"}"
    Env       = var.env
    App       = "opencast"
    Component = "administration"
    host      = "admin"
  }
}

#
# Edit
#
resource "aws_instance" "edit" {
  count         = var.edit_count
  ami           = local.opencast_ami_id
  instance_type = var.edit_type
  subnet_id     = element(var.subnet_ids.processing, count.index)
  key_name      = local.key_access_name
  root_block_device {
    volume_size = var.opencast_volume_size
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.publication
  user_data              = local.script_opencast_primary
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    // cloud-init only used for instance bootstrapping
    ignore_changes = [ ami, user_data ]
  }
  depends_on = [var.processing_dependences]
  tags = {
    Name      = "${var.prefix}-edit-${count.index}"
    Env       = var.env
    App       = "opencast"
    Component = "editing"
    host      = "edit"
  }
}

#
# Upload Scaling Group
#
resource "aws_launch_template" "upload" {
  name                   = "${var.prefix}-upload-${local.upload_ami_id}"
  image_id               = local.upload_ami_id
  instance_type          = var.upload_type
  key_name               = local.key_access_name
  vpc_security_group_ids = [var.security_group_ids.default]
  user_data              = base64encode(local.script_opencast_autoscale)
  iam_instance_profile {
    name = var.profile_names.upload
  }
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "null_resource" "tag_upload_ami" {
  triggers = {
    upload_ami_id = local.upload_ami_id
  }
  provisioner "local-exec" {
    environment = {
      AWS_PROFILE = var.prefix
      AWS_DEFAULT_REGION = var.region
      ami_name_pattern = "${var.prefix}-opencast-upload"
      ami_in_use_id = local.upload_ami_id
    }
    command = "${path.module}/scripts/tag_in_use_ami.sh"
  }
}

resource "aws_autoscaling_group" "upload" {
  name                 = "${var.prefix}-upload"
  max_size             = var.autoscale_upload_max
  min_size             = var.autoscale_upload_min
  desired_capacity     = null
  launch_template {
    id      = aws_launch_template.upload.id
    version = "$Latest"
  }
  vpc_zone_identifier  = var.subnet_ids.processing
  target_group_arns    = [aws_alb_target_group.upload.arn, aws_alb_target_group.upload_internal.arn]
  depends_on           = [var.processing_dependences]
  tags = [
    {
      key                 = "Name"
      value               = "${var.prefix}-upload"
      propagate_at_launch = true
    },
    {
      key                 = "Env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "App"
      value               = "opencast"
      propagate_at_launch = true
    },
    {
      key                 = "host"
      value               = "upload"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_lifecycle_hook" "upload_terminate" {
  name                   = "terminate"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = local.autoscale_upload_heartbeat
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"
}

resource "aws_autoscaling_policy" "upload" {
  name                   = "network-in"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  policy_type            = "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageNetworkIn"
    }
    target_value = var.autoscale_upload_network_in_target
  }
}

resource "aws_autoscaling_schedule" "upload_scale_in" {
  scheduled_action_name  = "scale-in-weekday"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  max_size               = var.autoscale_upload_quiet_max
  min_size               = var.autoscale_upload_quiet_min
  desired_capacity       = var.autoscale_upload_quiet_desired
  recurrence             = "0 18 * * MON-FRI"
  time_zone              = local.autoscale_timezone
}

resource "aws_autoscaling_schedule" "upload_scale_out" {
  scheduled_action_name  = "scale-out-weekday"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  max_size               = var.autoscale_upload_max
  min_size               = var.autoscale_upload_min
  desired_capacity       = var.autoscale_upload_desired
  recurrence             = "0 09 * * MON-FRI"
  time_zone              = local.autoscale_timezone
}

#
# Work Scaling Group
#
resource "aws_launch_template" "work" {
  name                   = "${var.prefix}-work-${local.work_ami_id}"
  image_id               = local.work_ami_id
  instance_type          = var.work_type
  key_name               = local.key_access_name
  vpc_security_group_ids = [var.security_group_ids.default]
  user_data              = base64encode(local.script_opencast_autoscale)
  iam_instance_profile {
    name = var.profile_names.work
  }
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "null_resource" "tag_work_ami" {
  triggers = {
    work_ami_id = local.work_ami_id
  }
  provisioner "local-exec" {
    environment = {
      AWS_PROFILE = var.prefix
      AWS_DEFAULT_REGION = var.region
      ami_name_pattern = "${var.prefix}-opencast-worker"
      ami_in_use_id = local.work_ami_id
    }
    command = "${path.module}/scripts/tag_in_use_ami.sh"
  }
}

resource "aws_autoscaling_group" "work" {
  name                 = "${var.prefix}-work"
  max_size             = var.autoscale_work_max
  min_size             = var.autoscale_work_min
  desired_capacity     = null
  launch_template {
    id      = aws_launch_template.work.id
    version = "$Latest"
  }
  vpc_zone_identifier  = var.subnet_ids.processing
  depends_on           = [var.processing_dependences]
  tags = [
    {
      key                 = "Name"
      value               = "${var.prefix}-work"
      propagate_at_launch = true
    },
    {
      key                 = "Env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "App"
      value               = "opencast"
      propagate_at_launch = true
    },
    {
      key                 = "host"
      value               = "work"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_policy" "work_cpu" {
  name                   = "cpu"
  autoscaling_group_name = aws_autoscaling_group.work.name
  policy_type            = "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = var.autoscale_work_cpu_target
  }
}

resource "aws_autoscaling_lifecycle_hook" "work_terminate" {
  name                   = "terminate"
  autoscaling_group_name = aws_autoscaling_group.work.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = local.autoscale_work_heartbeat
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"
}

resource "aws_autoscaling_schedule" "work_scale_in" {
  scheduled_action_name  = "scale-in-weekday"
  autoscaling_group_name = aws_autoscaling_group.work.name
  max_size               = var.autoscale_work_quiet_max
  min_size               = var.autoscale_work_quiet_min
  desired_capacity       = var.autoscale_work_quiet_desired
  recurrence             = "0 18 * * MON-FRI"
  time_zone              = local.autoscale_timezone
}

resource "aws_autoscaling_schedule" "work_scale_out" {
  scheduled_action_name  = "scale-out-weekday"
  autoscaling_group_name = aws_autoscaling_group.work.name
  max_size               = var.autoscale_work_max
  min_size               = var.autoscale_work_min
  desired_capacity       = var.autoscale_work_desired
  recurrence             = "0 09 * * MON-FRI"
  time_zone              = local.autoscale_timezone
}

#
# Whisper Scaling Group
#
resource "aws_launch_template" "whisper" {
  name                   = "${var.prefix}-whisper-${local.whisper_ami_id}"
  image_id               = local.whisper_ami_id
  instance_type          = var.whisper_gpu_type
  key_name               = local.key_access_name
  vpc_security_group_ids = [var.security_group_ids.default]
  user_data              = base64encode(local.script_opencast_autoscale)
  block_device_mappings  {
    device_name = "/dev/sda1"
    ebs {
      volume_size = 20
    }
  }
  iam_instance_profile {
    name = var.profile_names.whisper
  }
  lifecycle {
    create_before_destroy = true
  }
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }

}

resource "null_resource" "tag_whisper_ami" {
  triggers = {
    whisper_ami_id = local.whisper_ami_id
  }
  provisioner "local-exec" {
    environment = {
      AWS_PROFILE = var.prefix
      AWS_DEFAULT_REGION = var.region
      ami_name_pattern = "${var.prefix}-opencast-whisper"
      ami_in_use_id = local.whisper_ami_id
    }
    command = "${path.module}/scripts/tag_in_use_ami.sh"
  }
}

resource "aws_autoscaling_group" "whisper" {
  name                 = "${var.prefix}-whisper"
  max_size             = var.autoscale_whisper_max
  min_size             = var.autoscale_whisper_min
  desired_capacity     = var.autoscale_whisper_desired
  launch_template {
    id      = aws_launch_template.whisper.id
    version = "$Latest"
  }
  vpc_zone_identifier  = var.subnet_ids.processing

  warm_pool {
    pool_state                  = "Stopped"
    min_size                    = var.autoscale_whisper_warm_min
    max_group_prepared_capacity = var.autoscale_whisper_warm_max

    # This doesn't seem to work in our version of terraform
    # instance_reuse_policy {
    #   reuse_on_scale_in = true
    # }
  }

  tags = [
    {
      key                 = "Name"
      value               = "${var.prefix}-whisper-gpu"
      propagate_at_launch = true
    },
    {
      key                 = "Env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "App"
      value               = "opencast"
      propagate_at_launch = true
    },
    {
      key                 = "host"
      value               = "whisper"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_lifecycle_hook" "whisper_terminate" {
  name                   = "terminate"
  autoscaling_group_name = aws_autoscaling_group.whisper.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = local.autoscale_whisper_terminate_heartbeat
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"
}

resource "aws_autoscaling_lifecycle_hook" "whisper_launch" {
  name                   = "launch"
  autoscaling_group_name = aws_autoscaling_group.whisper.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = local.autoscale_whisper_launch_heartbeat
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_LAUNCHING"
}

