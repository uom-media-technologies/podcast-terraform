#@IgnoreInspection BashAddShebang
cfg_file=/opt/opencast/etc/custom.properties
tenant_file=/opt/opencast/etc/org.opencastproject.organization-mh_default_org.cfg
log_file=/opt/opencast/etc/org.ops4j.pax.logging.cfg
monitor_file=/etc/newrelic-infra.yml
httpd_file=/etc/httpd/conf/httpd.conf
httpd_opencast_file=/etc/httpd/conf.d/opencast.conf
node_hostname=$(curl http://169.254.169.254/latest/meta-data/local-hostname)
node_ip=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

tmp=$(mktemp)

# Set server url to this machines internal DNS
if [ -e $cfg_file ] ; then
    sed "s/^\(org\.opencastproject\.server\.url\=\).*/\1http:\/\/$node_hostname/g" $cfg_file > $tmp
    /bin/mv -f $tmp $cfg_file
fi
if [ -e $tenant_file ] ; then
    sed "s/^\(server\=\).*/\1$node_hostname/g" $tenant_file > $tmp
    /bin/mv -f $tmp $tenant_file
fi
if [ -e $httpd_file ] ; then
    sed "s/^\s*\(ServerName \).*/\1$node_hostname/g" $httpd_file > $tmp
    /bin/mv -f $tmp $httpd_file
fi
if [ -e $httpd_opencast_file ] ; then
    sed "s/^\s*\(ServerName \).*/\1$node_hostname/g" $httpd_opencast_file > $tmp
    /bin/mv -f $tmp $httpd_opencast_file
fi

# Append IP address to nodename to make it unique => node-0.0.0.0
if [ -e $cfg_file ] ; then
    sed "s/^\(org\.opencastproject\.server\.nodename\=[^-]*\).*/\1-$node_ip/g" $cfg_file > $tmp
    /bin/mv -f $tmp $cfg_file
fi
if [ -e $log_file ] ; then
    sed "s/^\(log4j2\.appender\.graylog\.layout\.host = [^-]*\).*/\1-$node_ip/g" $log_file > $tmp
    /bin/mv -f $tmp $log_file
fi
if [ -e $monitor_file ] ; then
    sed "s/^\(display_name: \"${environment_name} - [^-]*\).*/\1-$node_ip\"/g" $monitor_file > $tmp
    /bin/mv -f $tmp $monitor_file

    sed "s/^\(\s*node_name: \"[^-]*\).*/\1-$node_ip\"/g" $monitor_file > $tmp
    /bin/mv -f $tmp $monitor_file

    # Newrelic monitor may have already started
    systemctl restart newrelic-infra
fi
