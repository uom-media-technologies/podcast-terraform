# compute/delivery

# Video Portal and Tools

#
# Video
#
resource "aws_instance" "video" {
  count         = var.video_count
  ami           = local.video_ami_id
  instance_type = var.video_type
  subnet_id     = element(var.subnet_ids.delivery, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
    volume_size = var.video_volume_size
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.video
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    ignore_changes = [ami]
  }
  tags = {
    Name = "${var.prefix}-video-${count.index}"
    Env  = var.env
    App  = "videolounge"
    host = "video"
  }
}

resource "aws_volume_attachment" "video_index" {
  count = var.video_count
  device_name = "/dev/sdf"
  volume_id   = var.ebs_video_index_ids[count.index]
  instance_id = aws_instance.video[count.index].id
}

#
# Tools
#
resource "aws_instance" "tools" {
  count         = var.tools_count
  ami           = local.tools_ami_id
  instance_type = var.tools_type
  subnet_id     = element(var.subnet_ids.delivery, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
    volume_size = var.tools_volume_size
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.tools
  user_data              = local.script_primary
  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "optional"
  }
  lifecycle {
    // cloud-init only used for instance bootstrapping
    ignore_changes = [ ami, user_data ]
  }
  tags = {
    Name = "${var.prefix}-tools-${count.index}"
    Env  = var.env
    App  = "podcast tools"
    host = "tools"
  }
}

