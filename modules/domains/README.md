Domain names and aliases. Uses an existing hosted zone.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |

## Resources

| Name | Type |
|------|------|
| [aws_route53_zone.podcast](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_admin_ids"></a> [admin\_ids](#input\_admin\_ids) | Instance ids of primary and optional secondary admin instances | `list(string)` | n/a | yes |
| <a name="input_admin_ips"></a> [admin\_ips](#input\_admin\_ips) | Internal IPs of primary and optional secondary admin instances | `list(string)` | n/a | yes |
| <a name="input_admin_standby"></a> [admin\_standby](#input\_admin\_standby) | Failover to standby Opencast Admin instance | `bool` | `false` | no |
| <a name="input_cloudfront_distribution_domain"></a> [cloudfront\_distribution\_domain](#input\_cloudfront\_distribution\_domain) | Domain name of the cloudfront for distribution (not alias) | `string` | n/a | yes |
| <a name="input_database_dns_name"></a> [database\_dns\_name](#input\_database\_dns\_name) | Database auto-generated domain name | `any` | n/a | yes |
| <a name="input_env"></a> [env](#input\_env) | Deployed environment | `any` | n/a | yes |
| <a name="input_hosted_zone_name"></a> [hosted\_zone\_name](#input\_hosted\_zone\_name) | Primary hosted zone name | `any` | n/a | yes |
| <a name="input_index_ips"></a> [index\_ips](#input\_index\_ips) | Instance ids of primary and optional secondary index instances | `list(string)` | n/a | yes |
| <a name="input_lb_private_dns_name"></a> [lb\_private\_dns\_name](#input\_lb\_private\_dns\_name) | Domain name of internal loadbalancer | `any` | n/a | yes |
| <a name="input_lb_private_zone_id"></a> [lb\_private\_zone\_id](#input\_lb\_private\_zone\_id) | Hosted zone id of private loadbalancer | `any` | n/a | yes |
| <a name="input_lb_public_dns_name"></a> [lb\_public\_dns\_name](#input\_lb\_public\_dns\_name) | Domain name of public loadbalancer | `any` | n/a | yes |
| <a name="input_lb_public_https_arn"></a> [lb\_public\_https\_arn](#input\_lb\_public\_https\_arn) | ARN of public loadbalancer HTTPS listener | `any` | n/a | yes |
| <a name="input_lb_public_zone_id"></a> [lb\_public\_zone\_id](#input\_lb\_public\_zone\_id) | Hosted zone id of public loadbalancer | `any` | n/a | yes |
| <a name="input_lb_restricted_dns_name"></a> [lb\_restricted\_dns\_name](#input\_lb\_restricted\_dns\_name) | Domain name of restricted loadbalancer | `any` | n/a | yes |
| <a name="input_lb_restricted_https_arn"></a> [lb\_restricted\_https\_arn](#input\_lb\_restricted\_https\_arn) | ARN of restricted loadbalancer HTTPS listener | `any` | n/a | yes |
| <a name="input_lb_restricted_zone_id"></a> [lb\_restricted\_zone\_id](#input\_lb\_restricted\_zone\_id) | Hosted zone id of restricted loadbalancer | `any` | n/a | yes |
| <a name="input_log_ip"></a> [log\_ip](#input\_log\_ip) | IP of logging instance | `any` | n/a | yes |
| <a name="input_log_ip_internal"></a> [log\_ip\_internal](#input\_log\_ip\_internal) | Internal IP of logging instance | `any` | n/a | yes |
| <a name="input_ops_ip"></a> [ops\_ip](#input\_ops\_ip) | IP of operations instance | `any` | n/a | yes |
| <a name="input_ops_ip_internal"></a> [ops\_ip\_internal](#input\_ops\_ip\_internal) | Internal IP of operations instance | `any` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resource naming prefix | `any` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `any` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC identity | `any` | n/a | yes |

## Outputs

No outputs.
