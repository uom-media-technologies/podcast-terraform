# domains/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

# network inputs
variable "vpc_id" {
  description = "VPC identity"
}

# load balancers
variable "lb_public_https_arn" {
  description = "ARN of public loadbalancer HTTPS listener"
}

variable "lb_public_dns_name" {
  description = "Domain name of public loadbalancer"
}

variable "lb_public_zone_id" {
  description = "Hosted zone id of public loadbalancer"
}

variable "lb_restricted_https_arn" {
  description = "ARN of restricted loadbalancer HTTPS listener"
}

variable "lb_restricted_dns_name" {
  description = "Domain name of restricted loadbalancer"
}

variable "lb_restricted_zone_id" {
  description = "Hosted zone id of restricted loadbalancer"
}

variable "lb_private_dns_name" {
  description = "Domain name of internal loadbalancer"
}

variable "lb_private_zone_id" {
  description = "Hosted zone id of private loadbalancer"
}

variable "database_dns_name" {
  description = "Database auto-generated domain name"
}

variable "ops_ip" {
  description = "IP of operations instance"
}

variable "ops_ip_internal" {
  description = "Internal IP of operations instance"
}

variable "log_ip" {
  description = "IP of logging instance"
}

variable "log_ip_internal" {
  description = "Internal IP of logging instance"
}

variable "admin_ips" {
  type        = list(string)
  description = "Internal IPs of primary and optional secondary admin instances"
}

variable "index_ips" {
  type        = list(string)
  description = "Instance ids of primary and optional secondary index instances"
}

# default node numbers
variable "admin_standby" {
  default     = false
  description = "Failover to standby Opencast Admin instance"
}

variable "admin_ids" {
  type        = list(string)
  description = "Instance ids of primary and optional secondary admin instances"
}


variable "cloudfront_distribution_domain" {
  type        = string
  description = "Domain name of the cloudfront for distribution (not alias)"
}

# Domains configuration
variable "hosted_zone_name" {
  description = "Primary hosted zone name"
}

