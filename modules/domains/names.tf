# domains/names.tf

# Public Load balancers
resource "aws_route53_record" "edit" {
  zone_id = local.zone_id
  name    = "edit.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_public_dns_name
    zone_id                = var.lb_public_zone_id
  }
}

resource "aws_route53_record" "video" {
  zone_id = local.zone_id
  name    = "video.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_public_dns_name
    zone_id                = var.lb_public_zone_id
  }
}

resource "aws_route53_record" "tools" {
  zone_id = local.zone_id
  name    = "tools.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_public_dns_name
    zone_id                = var.lb_public_zone_id
  }
}

# Cloudfront alias
resource "aws_route53_record" "distribution" {
  zone_id = local.zone_id
  name    = "distribution.${var.env}.${var.hosted_zone_name}"
  type    = "CNAME"
  ttl     = 300
  records = [var.cloudfront_distribution_domain]
}

# Restricted Instances and Load balancers
resource "aws_route53_record" "admin" {
  zone_id = local.zone_id
  name    = "admin.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_restricted_dns_name
    zone_id                = var.lb_restricted_zone_id
  }
}

resource "aws_route53_record" "upload" {
  zone_id = local.zone_id
  name    = "upload.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_restricted_dns_name
    zone_id                = var.lb_restricted_zone_id
  }
}

resource "aws_route53_record" "ops" {
  zone_id = local.zone_id
  name    = "ops.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  ttl     = local.dns_ttl_instance
  records = [var.ops_ip]
}

resource "aws_route53_record" "log" {
  zone_id = local.zone_id
  name    = "log.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_restricted_dns_name
    zone_id                = var.lb_restricted_zone_id
  }
}

# Private Instances and Load balancers
resource "aws_route53_record" "admin_internal" {
  zone_id = local.internal_zone_id
  name    = "admin.${aws_route53_zone.internal.name}"
  type    = "A"
  ttl     = local.dns_ttl_instance
  records = [var.admin_ips[0]]
}

resource "aws_route53_record" "index_internal" {
  zone_id = local.internal_zone_id
  name    = "index.${aws_route53_zone.internal.name}"
  type    = "A"
  ttl     = local.dns_ttl_instance
  records = [var.index_ips[0]]
}

resource "aws_route53_record" "ops_internal" {
  zone_id = local.internal_zone_id
  name = "ops.${aws_route53_zone.internal.name}"
  type = "A"
  ttl = local.dns_ttl_instance
  records = [var.ops_ip_internal]
}

resource "aws_route53_record" "log_internal" {
  zone_id = local.internal_zone_id
  name    = "log.${var.env}.${var.hosted_zone_name}"
  type    = "A"
  ttl     = local.dns_ttl_instance
  records = [var.log_ip_internal]
}

resource "aws_route53_record" "edit_internal" {
  zone_id = local.internal_zone_id
  name    = "edit.${aws_route53_zone.internal.name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_private_dns_name
    zone_id                = var.lb_private_zone_id
  }
}

resource "aws_route53_record" "upload_internal" {
  zone_id = local.internal_zone_id
  name    = "upload.${aws_route53_zone.internal.name}"
  type    = "A"
  alias {
    evaluate_target_health = false
    name                   = var.lb_private_dns_name
    zone_id                = var.lb_private_zone_id
  }
}

resource "aws_route53_record" "db_internal" {
  zone_id = local.internal_zone_id
  name    = "db.${aws_route53_zone.internal.name}"
  type    = "CNAME"
  ttl     = local.dns_ttl_instance
  records = [var.database_dns_name]
}

resource "aws_route53_record" "distribution_internal" {
  zone_id = local.internal_zone_id
  name    = "distribution.${aws_route53_zone.internal.name}"
  type    = "CNAME"
  ttl     = local.dns_ttl_instance
  records = [var.cloudfront_distribution_domain]
}