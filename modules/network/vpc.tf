# network/vpc.tf

resource "aws_vpc" "default" {
  cidr_block           = local.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = var.prefix
    Env  = var.env
  }
}

#
# Internet Gateway
#
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id
  tags = {
    Name = var.prefix
    Env  = var.env
  }
}

#
# Private Gateway to Uni Network (via DX)
#
/*
resource "aws_vpn_gateway" "university" {
  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "${var.prefix}"
    Env = "${var.env}"
  }
}
resource "aws_dx_gateway_association" "example" {
  dx_gateway_id  = "${var.university_dx_gateway.id}"
  vpn_gateway_id = "${aws_vpn_gateway.university.id}"
}
*/

#
# NAT
#
data "aws_eip" "nat" {
  tags = {
    Name = "${var.prefix}-nat"
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = data.aws_eip.nat.id
  subnet_id     = aws_subnet.public_1.id

  depends_on = [aws_internet_gateway.default]
  tags = {
    Name = var.prefix
    Env  = var.env
  }
}

#
# Public Subnets Routing
#
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.default.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }
  tags = {
    Name = "${var.prefix}-public"
    Env  = var.env
  }
}

#
# Public Subnets
#
resource "aws_subnet" "public_1" {
  vpc_id                  = aws_vpc.default.id
  cidr_block              = local.subnet_public_1_cidr
  availability_zone       = element(var.zones, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.prefix}-public-1"
    Env  = var.env
  }
}

resource "aws_route_table_association" "public_1" {
  subnet_id      = aws_subnet.public_1.id
  route_table_id = aws_route_table.public.id
}

resource "aws_subnet" "public_2" {
  vpc_id                  = aws_vpc.default.id
  cidr_block              = local.subnet_public_2_cidr
  availability_zone       = element(var.zones, 1)
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.prefix}-public-2"
    Env  = var.env
  }
}

resource "aws_route_table_association" "public_2" {
  subnet_id      = aws_subnet.public_2.id
  route_table_id = aws_route_table.public.id
}

#
# Restricted Subnets Routing
#
resource "aws_route_table" "restricted" {
  vpc_id = aws_vpc.default.id
  tags = {
    Name = "${var.prefix}-restricted"
    Env  = var.env
  }
}

# UoM -> Uni Gateway
resource "aws_route" "restricted_uni" {
  count                  = length(var.uni_cidrs)
  route_table_id         = aws_route_table.restricted.id
  destination_cidr_block = element(var.uni_cidrs, count.index)
  gateway_id             = aws_internet_gateway.default.id # FIXME CHANGE to VPG
}

# UoM resources -> Uni Gateway
resource "aws_route" "restricted_resource" {
  count                  = length(var.resource_cidrs)
  route_table_id         = aws_route_table.restricted.id
  destination_cidr_block = element(var.resource_cidrs, count.index)
  gateway_id             = aws_internet_gateway.default.id # FIXME CHANGE to VPG
}

# Internet -> NAT
resource "aws_route" "restricted_internet" {
  route_table_id         = aws_route_table.restricted.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

#
# Restricted Subnets
#
resource "aws_subnet" "restricted_1" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = local.subnet_restricted_1_cidr
  availability_zone = element(var.zones, 0)
  tags = {
    Name = "${var.prefix}-restricted-1"
    Env  = var.env
  }
}

resource "aws_route_table_association" "restricted_1" {
  subnet_id      = aws_subnet.restricted_1.id
  route_table_id = aws_route_table.restricted.id
}

resource "aws_subnet" "restricted_2" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = local.subnet_restricted_2_cidr
  availability_zone = element(var.zones, 1)
  tags = {
    Name = "${var.prefix}-restricted-2"
    Env  = var.env
  }
}

resource "aws_route_table_association" "restricted_2" {
  subnet_id      = aws_subnet.restricted_2.id
  route_table_id = aws_route_table.restricted.id
}

#
# Private Subnets routing
#
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.default.id
  tags = {
    Name = "${var.prefix}-private"
    Env  = var.env
  }
}

# UoM resources -> Uni Gateway
resource "aws_route" "private_resource" {
  count                  = length(var.resource_cidrs)
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = element(var.resource_cidrs, count.index)
  gateway_id             = aws_internet_gateway.default.id # FIXME CHANGE to VPG
}

# Internet -> NAT
resource "aws_route" "private_internet" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

#
# Processing Subnet A
#
resource "aws_subnet" "processing_1" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = local.subnet_processing_1_cidr
  availability_zone = element(var.zones, 0)
  tags = {
    Name = "${var.prefix}-processing-1"
    Env  = var.env
  }
}

resource "aws_route_table_association" "processing_internal_1" {
  subnet_id      = aws_subnet.processing_1.id
  route_table_id = aws_route_table.private.id
}

#
# Processing Subnet B
#
resource "aws_subnet" "processing_2" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = local.subnet_processing_2_cidr
  availability_zone = element(var.zones, 1)
  tags = {
    Name = "${var.prefix}-processing-2"
    Env  = var.env
  }
}

resource "aws_route_table_association" "processing_internal_2" {
  subnet_id      = aws_subnet.processing_2.id
  route_table_id = aws_route_table.private.id
}

#
# Delivery Subnet A
#
resource "aws_subnet" "delivery_1" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = local.subnet_delivery_1_cidr
  availability_zone = element(var.zones, 0)
  tags = {
    Name = "${var.prefix}-delivery-1"
    Env  = var.env
  }
}

resource "aws_route_table_association" "delivery_internal_1" {
  subnet_id      = aws_subnet.delivery_1.id
  route_table_id = aws_route_table.private.id
}

#
# Delivery Subnet B
#
resource "aws_subnet" "delivery_2" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = local.subnet_delivery_2_cidr
  availability_zone = element(var.zones, 1)
  tags = {
    Name = "${var.prefix}-delivery-2"
    Env  = var.env
  }
}

resource "aws_route_table_association" "delivery_internal_2" {
  subnet_id      = aws_subnet.delivery_2.id
  route_table_id = aws_route_table.private.id
}

## S3 Gateway
resource "aws_vpc_endpoint" "s3" {
  vpc_id       = aws_vpc.default.id
  service_name = "com.amazonaws.${var.region}.s3"
  route_table_ids = [aws_route_table.private.id]
  tags = {
    Name = "${var.prefix}-s3"
    Env  = var.env
  }
}