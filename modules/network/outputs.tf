# network/outputs.tf

output "vpc_id" {
  value       = aws_vpc.default.id
  description = "VPC identity"
}

output "vpc_cidr" {
  value       = local.vpc_cidr
  description = "VPC subnet"
}

output "subnet_ids" {
  value = {
    public     = [aws_subnet.public_1.id, aws_subnet.public_2.id]
    restricted = [aws_subnet.restricted_1.id, aws_subnet.restricted_2.id]
    processing = [aws_subnet.processing_1.id, aws_subnet.processing_2.id]
    delivery   = [aws_subnet.delivery_1.id, aws_subnet.delivery_2.id]
  }
}


