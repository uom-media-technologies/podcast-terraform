# Network

Define VPC, subnets, routing and ACLs. VPC and subnets CIDRS defined as locals see network/main.tf

## Usage:
```
module "network" {
  source = "./network"
  prefix = "podcast-prod"
  env = "prod"
  zones = ["us-east-1", "us-west-1"]
  uni_cidrs = ["100.100.0.0/16"]
  resource_cidrs = ["10.200.0.0/16"]
}
```

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |

## Resources

| Name | Type |
|------|------|
| [aws_eip.nat](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eip) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_env"></a> [env](#input\_env) | Deployed environment | `any` | n/a | yes |
| <a name="input_monitoring_cidrs"></a> [monitoring\_cidrs](#input\_monitoring\_cidrs) | Subnets that will be have access for system/application monitoring | `list(string)` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resource naming prefix | `any` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `any` | n/a | yes |
| <a name="input_resource_cidrs"></a> [resource\_cidrs](#input\_resource\_cidrs) | University subnets that will be accessed by private subnets | `list(string)` | n/a | yes |
| <a name="input_uni_cidrs"></a> [uni\_cidrs](#input\_uni\_cidrs) | University subnets that can access restricted subnets | `list(string)` | n/a | yes |
| <a name="input_zones"></a> [zones](#input\_zones) | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_subnet_ids"></a> [subnet\_ids](#output\_subnet\_ids) | n/a |
| <a name="output_vpc_cidr"></a> [vpc\_cidr](#output\_vpc\_cidr) | VPC subnet |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | VPC identity |
