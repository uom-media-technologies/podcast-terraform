# network/acls.tf
/*
#
# ACLs for Subnet Restricted ===========================================================================================
#
resource "aws_network_acl" "restricted" {
  vpc_id = "${aws_vpc.default.id}"
  subnet_ids =  ["${aws_subnet.restricted_1.id}", "${aws_subnet.restricted_2.id}"]
  tags = {
    Name = "${var.prefix}-restricted"
  }
}

# HTTP to UoM
resource "aws_network_acl_rule" "restricted-uni-http-inbound" {
  count = "${length(var.uni_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${100 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.uni_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "restricted-uni-http-outbound" {
  count = "${length(var.uni_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${100 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.uni_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# HTTP to processing
resource "aws_network_acl_rule" "restricted-http-inbound" {
  count = "${length(local.subnet_processing_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${200 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_processing_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "restricted-http-outbound" {
  count = "${length(local.subnet_processing_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${200 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_processing_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# HTTP to Monitoring
resource "aws_network_acl_rule" "restricted-monitoring-http-inbound" {
  count = "${length(var.monitoring_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${1000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.monitoring_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "restricted-monitoring-http-outbound" {
  count = "${length(var.monitoring_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${1000 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.monitoring_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# HTTP to Internet
resource "aws_network_acl_rule" "restricted-internet-http-inbound" {
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = 2000
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "restricted-internet-http-outbound" {
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = 2000
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port = 443
  to_port = 443
}

# SSH
resource "aws_network_acl_rule" "restricted-uni-ssh-inbound" {
  count = "${length(var.uni_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${4000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.uni_cidrs, count.index)}"
  from_port = 22
  to_port = 22
}

# Ephemeral ports from uni
resource "aws_network_acl_rule" "restricted-uni-ssh-outbound" {
  count = "${length(var.uni_cidrs)}"
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${4000 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.uni_cidrs, count.index)}"
  from_port = 32768
  to_port = 61000
}

# Restricted can ssh rest of VPC
resource "aws_network_acl_rule" "restricted-ssh-outbound" {
  network_acl_id = "${aws_network_acl.restricted.id}"
  rule_number = "${5000 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${local.vpc_cidr}"
  from_port = 22
  to_port = 22
}

#
# ACLs for Subnet Processing ===========================================================================================
#
resource "aws_network_acl" "processing" {
  vpc_id = "${aws_vpc.default.id}"
  subnet_ids = ["${aws_subnet.processing_1.id}", "${aws_subnet.processing_2.id}"]
  tags = {
    Name = "${var.prefix}-processing"
  }
}

# HTTP to restricted
resource "aws_network_acl_rule" "processing-http-inbound" {
  count = "${length(local.subnet_restricted_cidrs)}"
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = "${200 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_restricted_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "processing-http-outbound" {
  count = "${length(local.subnet_restricted_cidrs)}"
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = "${200 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_restricted_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# MSSQL to UoM
resource "aws_network_acl_rule" "processing-uni-mssql-inbound" {
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = 400
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "10.0.0.0/8" # TODO lock this down
  from_port = 1433
  to_port = 1433
}
resource "aws_network_acl_rule" "processing-uni-mssql-outbound" {
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = 400
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "10.0.0.0/8" # TODO lock this down
  from_port = 1433
  to_port = 1433
}

# HTTP to Monitoring
resource "aws_network_acl_rule" "processing-monitoring-http-inbound" {
  count = "${length(var.monitoring_cidrs)}"
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = "${1000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.monitoring_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "processing-monitoring-http-outbound" {
  count = "${length(var.monitoring_cidrs)}"
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = "${1000 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.monitoring_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# HTTP to Internet
resource "aws_network_acl_rule" "processing-internet-http-inbound" {
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = 2000
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "processing-internet-http-outbound" {
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = 2000
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port = 443
  to_port = 443
}

# SSH from restricted
resource "aws_network_acl_rule" "processing-ssh-inbound" {
  count = "${length(local.subnet_restricted_cidrs)}"
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = "${4000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_restricted_cidrs, count.index)}"
  from_port = 22
  to_port = 22
}

# Ephemeral ports from  restricted
resource "aws_network_acl_rule" "processing-ssh-outbound" {
  count = "${length(local.subnet_restricted_cidrs)}"
  network_acl_id = "${aws_network_acl.processing.id}"
  rule_number = "${5000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_restricted_cidrs, count.index)}"
  from_port = 32768
  to_port = 61000
}

#
# ACLs for Subnet Delivery =============================================================================================
#
resource "aws_network_acl" "delivery" {
  vpc_id = "${aws_vpc.default.id}"
  subnet_ids = ["${aws_subnet.delivery_1.id}", "${aws_subnet.delivery_2.id}"]
  tags = {
    Name = "${var.prefix}-delivery"
  }
}

# HTTP to public
resource "aws_network_acl_rule" "delivery-http-inbound" {
  count = "${length(local.subnet_public_cidrs)}"
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = "${200 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_public_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "delivery-http-outbound" {
  count = "${length(local.subnet_public_cidrs)}"
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = "${200 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_public_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# MSSQL to UoM
resource "aws_network_acl_rule" "delivery-uni-mssql-inbound" {
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = 400
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "10.0.0.0/8" # TODO lock this down
  from_port = 1433
  to_port = 1433
}
resource "aws_network_acl_rule" "delivery-uni-mssql-outbound" {
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = 400
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "10.0.0.0/8" # TODO lock this down
  from_port = 1433
  to_port = 1433
}


# HTTP to Monitoring
resource "aws_network_acl_rule" "delivery-monitoring-http-inbound" {
  count = "${length(local.subnet_delivery_cidrs)}"
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = "${1000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.monitoring_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "delivery-monitoring-http-outbound" {
  count = "${length(var.monitoring_cidrs)}"
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = "${1000 + 5*count.index}"
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(var.monitoring_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# SSH from restricted
resource "aws_network_acl_rule" "delivery-ssh-inbound" {
  count = "${length(local.subnet_restricted_cidrs)}"
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = "${4000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_restricted_cidrs, count.index)}"
  from_port = 443
  to_port = 443
}

# Ephemeral ports from  restricted
resource "aws_network_acl_rule" "delivery-ssh-outbound" {
  count = "${length(local.subnet_restricted_cidrs)}"
  network_acl_id = "${aws_network_acl.delivery.id}"
  rule_number = "${5000 + 5*count.index}"
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "${element(local.subnet_restricted_cidrs, count.index)}"
  from_port = 32768
  to_port = 61000
}

#
# ACLs for Subnet Public ===============================================================================================
#
resource "aws_network_acl" "public" {
  vpc_id = "${aws_vpc.default.id}"
  subnet_ids = ["${aws_subnet.public_1.id}", "${aws_subnet.public_2.id}"]
  tags = {
    Name = "${var.prefix}-public"
  }
}

# HTTP to internet
resource "aws_network_acl_rule" "public-http-inbound" {
  network_acl_id = "${aws_network_acl.public.id}"
  rule_number = 100
  egress = false
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port = 443
  to_port = 443
}
resource "aws_network_acl_rule" "public-http-outbound" {
  network_acl_id = "${aws_network_acl.public.id}"
  rule_number = 100
  egress = true
  protocol = "tcp"
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port = 443
  to_port = 443
}
*/
