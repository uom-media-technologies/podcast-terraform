# security/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

variable "account" {
  description = "Account number, for ARN calculation"
}

# network inputs
variable "vpc_id" {
  description = "VPC identity"
}

variable "vpc_cidr" {
  description = "VPC subnet"
}

variable "uni_cidrs" {
  type        = list(string)
  description = "University subnets that can access restricted subets"
}

variable "resource_cidrs" {
  type        = list(string)
  description = "University subnets that will be accessed by private subnets"
}

variable "hosted_zone_name" {
  description = "Primary hosted zone name"
}

# keys
variable "bastion_public_key" {
  description = "Public key to for access to the bastion instance"
}

variable "access_public_key" {
  description = "Public key to for access to internal instances"
}
