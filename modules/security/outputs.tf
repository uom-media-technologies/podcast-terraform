# security/outputs.tf

output "security_group_ids" {
  value = {
    default    = data.aws_security_group.default.id
    users      = aws_security_group.users.id
    nat        = aws_security_group.nat.id
    restricted = aws_security_group.restricted.id
    bastion    = aws_security_group.bastion.id
  }
  description = "Security group ids"
}

output "profile_names" {
  value = {
    admin       = aws_iam_instance_profile.admin.name
    publication = aws_iam_instance_profile.publication.name
    upload      = aws_iam_instance_profile.upload.name
    work        = aws_iam_instance_profile.work.name
    video       = aws_iam_instance_profile.video.name
    tools       = aws_iam_instance_profile.tools.name
    ops         = aws_iam_instance_profile.ops.name
    whisper     = aws_iam_instance_profile.whisper.name
  }
  description = "Names of instance profiles (roles)"
}

output "certificate_arns" {
  value = {
    admin        = aws_acm_certificate_validation.admin.certificate_arn
    edit         = aws_acm_certificate_validation.edit.certificate_arn
    upload       = aws_acm_certificate_validation.ingest.certificate_arn
    video        = aws_acm_certificate_validation.video.certificate_arn
    distribution = aws_acm_certificate_validation.distribution.certificate_arn
    tools        = aws_acm_certificate_validation.tools.certificate_arn
    log          = aws_acm_certificate_validation.log.certificate_arn
  }
  description = "ARNs of SSL certificates"
}

output "waf_arn" {
  value       = aws_wafv2_web_acl.podcast.arn
  description = "ARN of the Web Application Firewall"
}
