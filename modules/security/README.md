# Security

Define IAM roles and groups.

## Usage:
```
module "security" {
  source = "./security"
  prefix = "podcast-prod"
  env = "prod"
  region = "us-east"
  zones = ["us-east-1", "us-west-1"]
  key_name = "podcast"
  account = "12345678901"

  vpc_id = "${module.network.vpc_id}"
  vpc_cidr = "${module.network.vpc_cidr}"
  protocol_external = "HTTPS"
  uni_cidrs = ["100.100.0.0/16"]
  resource_cidrs = ["10.200.0.0/16"]
  monitoring_cidrs = "[12.34.56.0/24]"

  subnet_public_ids = "${module.network.subnet_public_ids}"
  subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
  subnet_processing_ids = "${module.network.subnet_processing_ids}"
  subnet_delivery_ids = "${module.network.subnet_delivery_ids}"
  subnet_public_cidrs = "${module.network.subnet_public_cidrs}"
  subnet_restricted_cidrs = "${module.network.subnet_restricted_cidrs}"
  subnet_processing_cidrs = "${module.network.subnet_processing_cidrs}"
  subnet_delivery_cidrs = "${module.network.subnet_delivery_cidrs}"
}
```

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~>3.0 |
| <a name="provider_aws.acm"></a> [aws.acm](#provider\_aws.acm) | ~>3.0 |

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.cloudwatch_agent](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_iam_policy.ec2_readonly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_iam_policy.rds_readonly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_iam_policy.route53_readonly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_route53_zone.podcast](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [aws_security_group.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/security_group) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_access_public_key"></a> [access\_public\_key](#input\_access\_public\_key) | Public key to for access to internal instances | `any` | n/a | yes |
| <a name="input_account"></a> [account](#input\_account) | Account number, for ARN calculation | `any` | n/a | yes |
| <a name="input_bastion_public_key"></a> [bastion\_public\_key](#input\_bastion\_public\_key) | Public key to for access to the bastion instance | `any` | n/a | yes |
| <a name="input_env"></a> [env](#input\_env) | Deployed environment | `any` | n/a | yes |
| <a name="input_hosted_zone_name"></a> [hosted\_zone\_name](#input\_hosted\_zone\_name) | Primary hosted zone name | `any` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Resource naming prefix | `any` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `any` | n/a | yes |
| <a name="input_resource_cidrs"></a> [resource\_cidrs](#input\_resource\_cidrs) | University subnets that will be accessed by private subnets | `list(string)` | n/a | yes |
| <a name="input_uni_cidrs"></a> [uni\_cidrs](#input\_uni\_cidrs) | University subnets that can access restricted subets | `list(string)` | n/a | yes |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC subnet | `any` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC identity | `any` | n/a | yes |
| <a name="input_zones"></a> [zones](#input\_zones) | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_certificate_arns"></a> [certificate\_arns](#output\_certificate\_arns) | ARNs of SSL certificates |
| <a name="output_profile_names"></a> [profile\_names](#output\_profile\_names) | Names of instance profiles (roles) |
| <a name="output_security_group_ids"></a> [security\_group\_ids](#output\_security\_group\_ids) | Security group ids |
| <a name="output_waf_arn"></a> [waf\_arn](#output\_waf\_arn) | ARN of the Web Application Firewall |
