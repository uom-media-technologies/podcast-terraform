# security/keys.tf

resource "aws_key_pair" "bastion" {
  key_name   = "${var.prefix}-bastion"
  public_key = var.bastion_public_key
}

resource "aws_key_pair" "ansible" {
  key_name   = "${var.prefix}-access"
  public_key = var.access_public_key
}

