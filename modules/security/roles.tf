# security/roles.tf

# policies, roles and profiles

#
# Write logs to S3 bucket
#
data "aws_iam_policy" "cloudwatch_agent" {
  arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

#
# Distribution Service
#
resource "aws_iam_role" "publication" {
  name               = "${var.prefix}-publication"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF

  tags = {
    Env = var.env
  }
}

# A container that can be associated with an instance and has a 1:1 relationship with a role
resource "aws_iam_instance_profile" "publication" {
  name = "${var.prefix}-publication"
  role = aws_iam_role.publication.name
}

resource "aws_iam_role_policy_attachment" "log_publication" {
  role       = aws_iam_role.publication.name
  policy_arn = data.aws_iam_policy.cloudwatch_agent.arn
}

# Distribution Service <=> S3 Distribrute
resource "aws_iam_policy" "s3_distribute_rw" {
  name        = "${var.prefix}-s3-distribute-rw"
  description = "Allow entity to CRUD objects in S3 publication bucket"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "S3List",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:HeadBucket"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "S3ReadWrite",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${var.prefix}-distribution",
        "arn:aws:s3:::${var.prefix}-distribution/*"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "s3_publication" {
  role       = aws_iam_role.publication.name
  policy_arn = aws_iam_policy.s3_distribute_rw.arn
}

#
# Archive Service
#
resource "aws_iam_role" "admin" {
  name               = "${var.prefix}-admin"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF


  tags = {
    Env = var.env
  }
}

# A container that can be associated with an instance and has a 1:1 relationship with a role
resource "aws_iam_instance_profile" "admin" {
  name = "${var.prefix}-admin"
  role = aws_iam_role.admin.name
}

resource "aws_iam_role_policy_attachment" "log_admin" {
  role       = aws_iam_role.admin.name
  policy_arn = data.aws_iam_policy.cloudwatch_agent.arn
}

# Archive Service <=> S3 Archive
resource "aws_iam_policy" "s3_archive_rw" {
  name        = "${var.prefix}-s3-archive-rw"
  description = "Allow entity to CRUD objects in S3 archive bucket"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "S3List",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:HeadBucket"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "S3ReadWrite",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${var.prefix}-archive",
        "arn:aws:s3:::${var.prefix}-archive/*"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "s3_admin" {
  role       = aws_iam_role.admin.name
  policy_arn = aws_iam_policy.s3_archive_rw.arn
}


# Archive S3 Batch Operations Role
resource "aws_iam_role" "batch" {
  name               = "${var.prefix}-archive-s3-batch"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "batchoperations.s3.amazonaws.com"
      }
    }
  ]
}
EOF

  tags = {
    Env = var.env
  }
}

resource "aws_iam_role_policy_attachment" "s3_batch" {
  role       = aws_iam_role.batch.name
  policy_arn = aws_iam_policy.s3_archive_rw.arn
}


# Archive Service <=> Glacier Archive
resource "aws_iam_policy" "glacier_archive_rw" {
  name        = "${var.prefix}-glacier-archive-rw"
  description = "Allow entity to CRUD objects in Glacier archive vault"
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "GlacierArchiveListReadWrite",
            "Effect": "Allow",
            "Action": [
                "glacier:AbortMultipartUpload",
                "glacier:GetVaultAccessPolicy",
                "glacier:CreateVault",
                "glacier:DescribeVault",
                "glacier:ListParts",
                "glacier:GetVaultNotifications",
                "glacier:DescribeJob",
                "glacier:DeleteVaultNotifications",
                "glacier:GetDataRetrievalPolicy",
                "glacier:ListJobs",
                "glacier:InitiateMultipartUpload",
                "glacier:PurchaseProvisionedCapacity",
                "glacier:UploadArchive",
                "glacier:InitiateJob",
                "glacier:ListTagsForVault",
                "glacier:DeleteVault",
                "glacier:DeleteArchive",
                "glacier:AddTagsToVault",
                "glacier:GetJobOutput",
                "glacier:ListMultipartUploads",
                "glacier:SetVaultNotifications",
                "glacier:RemoveTagsFromVault",
                "glacier:CompleteMultipartUpload",
                "glacier:UploadMultipartPart",
                "glacier:GetVaultLock",
                "glacier:ListVaults",
                "glacier:ListProvisionedCapacity"
            ],
            "Resource": "arn:aws:glacier:${var.region}:${var.account}:vaults/${var.prefix}-archive"
        }
    ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "glacier_admin" {
  role       = aws_iam_role.admin.name
  policy_arn = aws_iam_policy.glacier_archive_rw.arn
}

#
# Delivery Service
#
resource "aws_iam_role" "video" {
  name               = "${var.prefix}-video"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF


  tags = {
    Env = var.env
  }
}

# A container that can be associated with an instance and has a 1:1 relationship with a role
resource "aws_iam_instance_profile" "video" {
  name = "${var.prefix}-video"
  role = aws_iam_role.video.name
}

resource "aws_iam_role_policy_attachment" "log_video" {
  role       = aws_iam_role.video.name
  policy_arn = data.aws_iam_policy.cloudwatch_agent.arn
}

# Delivery Service <= S3 Distribute
resource "aws_iam_policy" "s3_distribute_ro" {
  name        = "${var.prefix}-s3-distribute-ro"
  description = "Allow entity to READ objects in S3 publication bucket"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "S3List",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:HeadBucket"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "S3ReadOnly",
      "Action": [
        "s3:Get*",
        "s3:List*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::${var.prefix}-distribution",
        "arn:aws:s3:::${var.prefix}-distribution/*"
      ]
    }
  ]
}
EOF

}


resource "aws_iam_policy" "s3_logs_wo" {
  name        = "${var.prefix}-s3-logs-wo"
  description = "Allow entity to Write objects in S3 logs bucket"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "S3List",
      "Action": [
        "s3:HeadBucket"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "S3WriteOnly",
      "Action": [
        "s3:Put*",
        "s3:List*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::media-technologies-logs",
        "arn:aws:s3:::media-technologies-logs/*"
      ]
    }
  ]
}
EOF

}

# Delivery Service <= Cloudfront Distribute
resource "aws_iam_policy" "cloudfront_distribute_ro" {
  name        = "${var.prefix}-cloudfront-distribute-ro"
  description = "Allow entity to list cloudfront distribute public keys"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "CloudfrontPublicKeysList",
      "Action": [
        "cloudfront:ListPublicKeys"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  tags = {
    Env = var.env
  }
}

resource "aws_iam_role_policy_attachment" "s3_video" {
  role       = aws_iam_role.video.name
  policy_arn = aws_iam_policy.s3_distribute_ro.arn
}

resource "aws_iam_role_policy_attachment" "s3_logs_video" {
  role       = aws_iam_role.video.name
  policy_arn = aws_iam_policy.s3_logs_wo.arn
}

resource "aws_iam_role_policy_attachment" "cloudfront_video" {
  role       = aws_iam_role.video.name
  policy_arn = aws_iam_policy.cloudfront_distribute_ro.arn
}


#
# Opencast Worker and Ingest Services
#
resource "aws_iam_role" "work" {
  name               = "${var.prefix}-work"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF


  tags = {
    Env = var.env
  }
}

resource "aws_iam_instance_profile" "work" {
  name = "${var.prefix}-work"
  role = aws_iam_role.work.name
}

#
# Opencast Whisper Services
#
resource "aws_iam_role" "whisper" {
  name               = "${var.prefix}-whisper"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF


  tags = {
    Env = var.env
  }
}

resource "aws_iam_instance_profile" "whisper" {
  name = "${var.prefix}-whisper"
  role = aws_iam_role.whisper.name
}

resource "aws_iam_role" "upload" {
  name               = "${var.prefix}-upload"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF


  tags = {
    Env = var.env
  }
}

resource "aws_iam_instance_profile" "upload" {
  name = "${var.prefix}-upload"
  role = aws_iam_role.upload.name
}

resource "aws_iam_role_policy_attachment" "log_upload" {
  role       = aws_iam_role.admin.name
  policy_arn = data.aws_iam_policy.cloudwatch_agent.arn
}

# autoscaling lifecycle policy
resource "aws_iam_policy" "autoscaling_lifecycle" {
  name        = "${var.prefix}-autoscaling-lifecycle"
  description = "Allow entity to interact with Autoscaling Lifecycle"
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ReadInstanceLifcycle",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeLifecycleHooks"
            ],
            "Resource": "*"
        },
        {
            "Sid": "UpdateLifcycle",
            "Effect": "Allow",
            "Action": [
                "autoscaling:CompleteLifecycleAction",
                "autoscaling:RecordLifecycleActionHeartbeat"
            ],
            "Resource": "arn:aws:autoscaling:${var.region}:${var.account}:autoScalingGroup:*:autoScalingGroupName/*"
        }
    ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "autoscaling_lifecycle_work" {
  role       = aws_iam_role.work.name
  policy_arn = aws_iam_policy.autoscaling_lifecycle.arn
}

resource "aws_iam_role_policy_attachment" "autoscaling_lifecycle_upload" {
  role       = aws_iam_role.upload.name
  policy_arn = aws_iam_policy.autoscaling_lifecycle.arn
}

resource "aws_iam_role_policy_attachment" "autoscaling_lifecycle_whisper" {
  role       = aws_iam_role.whisper.name
  policy_arn = aws_iam_policy.autoscaling_lifecycle.arn
}

#
# Ansible Inventory Service
#
resource "aws_iam_role" "ops" {
  name               = "${var.prefix}-ops"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF

  tags = {
    Env = var.env
  }
}

# A container that can be associated with an instance and has a 1:1 relationship with a role
resource "aws_iam_instance_profile" "ops" {
  name = "${var.prefix}-ops"
  role = aws_iam_role.ops.name
}

#
# Tools
#
resource "aws_iam_role" "tools" {
  name               = "${var.prefix}-tools"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AssumeRole",
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF

  tags = {
    Env = var.env
  }
}

# A container that can be associated with an instance and has a 1:1 relationship with a role
resource "aws_iam_instance_profile" "tools" {
  name = "${var.prefix}-tools"
  role = aws_iam_role.tools.name
}

resource "aws_iam_role_policy_attachment" "s3_tools" {
  role       = aws_iam_role.tools.name
  policy_arn = aws_iam_policy.s3_distribute_ro.arn
}

resource "aws_iam_role_policy_attachment" "log_tools" {
  role       = aws_iam_role.tools.name
  policy_arn = data.aws_iam_policy.cloudwatch_agent.arn
}


# Ansible Inventory Service <= EC2, RDS and Route53 use AWS managed polices
data "aws_iam_policy" "ec2_readonly" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
}

data "aws_iam_policy" "rds_readonly" {
  arn = "arn:aws:iam::aws:policy/AmazonRDSReadOnlyAccess"
}

data "aws_iam_policy" "route53_readonly" {
  arn = "arn:aws:iam::aws:policy/AmazonRoute53ReadOnlyAccess"
}

data "aws_iam_policy" "ssm" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_policy" "secrets_readonly" {
  name = "${var.prefix}-secrets-readonly"
  description = "Allow readonly access to ${var.env} secrets"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "secretsmanager:GetSecretValue",
        Resource = [
          "arn:aws:secretsmanager:eu-west-2:600755374209:secret:media/podcasting/${var.env}/*",
          "arn:aws:secretsmanager:eu-west-2:600755374209:secret:media/podcasting/live/*",
          "arn:aws:secretsmanager:eu-west-2:600755374209:secret:opencast/podcasting/live/*"
        ]
      }
    ]
  })
  tags = {
    Env = var.env
  }
}

resource "aws_iam_role_policy_attachment" "ec2_ops" {
  role       = aws_iam_role.ops.name
  policy_arn = data.aws_iam_policy.ec2_readonly.arn
}

resource "aws_iam_role_policy_attachment" "rds_ops" {
  role       = aws_iam_role.ops.name
  policy_arn = data.aws_iam_policy.rds_readonly.arn
}

resource "aws_iam_role_policy_attachment" "route53_ops" {
  role       = aws_iam_role.ops.name
  policy_arn = data.aws_iam_policy.route53_readonly.arn
}

resource "aws_iam_role_policy_attachment" "ssm_ops" {
  role       = aws_iam_role.ops.name
  policy_arn = data.aws_iam_policy.ssm.arn
}

resource "aws_iam_role_policy_attachment" "secrets_ops" {
  role       = aws_iam_role.ops.name
  policy_arn = aws_iam_policy.secrets_readonly.arn
}
