# security/certificates.tf
data "aws_route53_zone" "podcast" {
  name = var.hosted_zone_name
}

locals {
  zone_id = data.aws_route53_zone.podcast.id
}

# Admin SSL
resource "aws_acm_certificate" "admin" {
  domain_name       = "admin.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "admin_cert_validation" {
  name    = tolist(aws_acm_certificate.admin.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.admin.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.admin.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "admin" {
  certificate_arn         = aws_acm_certificate.admin.arn
  validation_record_fqdns = [aws_route53_record.admin_cert_validation.fqdn]
}

# Ingest SSL
resource "aws_acm_certificate" "ingest" {
  domain_name       = "upload.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "ingest_cert_validation" {
  name    = tolist(aws_acm_certificate.ingest.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.ingest.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.ingest.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "ingest" {
  certificate_arn         = aws_acm_certificate.ingest.arn
  validation_record_fqdns = [aws_route53_record.ingest_cert_validation.fqdn]
}

# Edit SSL
resource "aws_acm_certificate" "edit" {
  domain_name       = "edit.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "edit_cert_validation" {
  name    = tolist(aws_acm_certificate.edit.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.edit.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.edit.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "edit" {
  certificate_arn         = aws_acm_certificate.edit.arn
  validation_record_fqdns = [aws_route53_record.edit_cert_validation.fqdn]
}


# Log SSL
resource "aws_acm_certificate" "log" {
  domain_name       = "log.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "log_cert_validation" {
  name    = tolist(aws_acm_certificate.log.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.log.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.log.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "log" {
  certificate_arn         = aws_acm_certificate.log.arn
  validation_record_fqdns = [aws_route53_record.log_cert_validation.fqdn]
}

# Video SSL
resource "aws_acm_certificate" "video" {
  domain_name       = "video.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "video_cert_validation" {
  name    = tolist(aws_acm_certificate.video.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.video.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.video.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "video" {
  certificate_arn         = aws_acm_certificate.video.arn
  validation_record_fqdns = [aws_route53_record.video_cert_validation.fqdn]
}

# Tools SSL
resource "aws_acm_certificate" "tools" {
  domain_name       = "tools.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "tools_cert_validation" {
  name    = tolist(aws_acm_certificate.tools.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.tools.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.tools.domain_validation_options)[0].resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "tools" {
  certificate_arn         = aws_acm_certificate.tools.arn
  validation_record_fqdns = [aws_route53_record.tools_cert_validation.fqdn]
}

# Cloudfront CDN
# NOTE Cloudfront SSL certs must be held by the AWS SCM in us-east-1
resource "aws_acm_certificate" "distribution" {
  provider          = aws.acm
  domain_name       = "distribution.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
  tags = {
    Name = "${var.prefix}-distribution"
    Env  = var.env
    App  = "cloudfront"
  }
}

resource "aws_route53_record" "distribution_cert_validation" {
  name    = tolist(aws_acm_certificate.distribution.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.distribution.domain_validation_options)[0].resource_record_type
  zone_id = local.zone_id
  records = [tolist(aws_acm_certificate.distribution.domain_validation_options)[0].resource_record_value]
  ttl     = 300
}

resource "aws_acm_certificate_validation" "distribution" {
  provider                = aws.acm
  certificate_arn         = aws_acm_certificate.distribution.arn
  validation_record_fqdns = [aws_route53_record.distribution_cert_validation.fqdn]
}