/**
 * # Security
 *
 * Define IAM roles and groups.
 *
 * ## Usage:
 * ```
 * module "security" {
 *   source = "./security"
 *   prefix = "podcast-prod"
 *   env = "prod"
 *   region = "us-east"
 *   zones = ["us-east-1", "us-west-1"]
 *   key_name = "podcast"
 *   account = "12345678901"
 *
 *   vpc_id = "${module.network.vpc_id}"
 *   vpc_cidr = "${module.network.vpc_cidr}"
 *   protocol_external = "HTTPS"
 *   uni_cidrs = ["100.100.0.0/16"]
 *   resource_cidrs = ["10.200.0.0/16"]
 *   monitoring_cidrs = "[12.34.56.0/24]"
 *
 *   subnet_public_ids = "${module.network.subnet_public_ids}"
 *   subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
 *   subnet_processing_ids = "${module.network.subnet_processing_ids}"
 *   subnet_delivery_ids = "${module.network.subnet_delivery_ids}"
 *   subnet_public_cidrs = "${module.network.subnet_public_cidrs}"
 *   subnet_restricted_cidrs = "${module.network.subnet_restricted_cidrs}"
 *   subnet_processing_cidrs = "${module.network.subnet_processing_cidrs}"
 *   subnet_delivery_cidrs = "${module.network.subnet_delivery_cidrs}"
 * }
 * ```
 */

#Only used for accessing certificates in the primary AWS region
provider "aws" {
  alias = "acm"
  region  = "us-east-1"
  profile = var.prefix
}


