/*
variable "service" {
  default = "podcast"
}
variable "env" {
  default = "stage"
}

variable "region" {
  default = "eu-west-1"
}
variable "zones" {
  type = "list" # only needed to keep pycharm happy, as it can be inferred
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

variable "protocol_external" {
  default = "HTTP"
}

variable "protocol_internal" {
  default = "HTTP"
}

variable "uni_cidrs" {
  type = "list"
  # 1 Campus
  # 2 & 3 GP VPN
  default = ["130.88.0.0/16", "10.240.0.0/13", "172.24.0.0/19"]
}
variable "resource_cidrs" {
  type = "list"
  default = ["${local.timetable_cidrs}", "${local.dass_cidrs}"]
}
variable "monitoring_cidrs" {
  type = "list"
  # 1 NewRelic Infrastructure Agents EU, https://docs.newrelic.com/docs/apm/new-relic-apm/getting-started/networks
  default = ["185.221.84.0/22"]
}
*/

locals {
  service = "podcast"
  env     = "dev"
  region  = "eu-west-2"
  zones   = ["${local.region}a", "${local.region}b", "${local.region}c"]
}

locals {
  timetable_cidr   = "10.0.0.0/8"
  dass_cidr        = "10.1.0.0/16"
  uni_cidrs        = ["130.88.0.0/16", "10.240.0.0/13", "172.24.0.0/19"]
  resource_cidrs   = [local.timetable_cidr, local.dass_cidr]
  monitoring_cidrs = ["185.221.84.0/22"]
}

locals {
  parent_domain = "media.its.manchester.ac.uk"
  env_domain    = "${local.env}.${local.parent_domain}"
  uni_domains   = ["manchester.ac.uk", "*.manchester.ac.uk", "mbs.ac.uk"]
}
